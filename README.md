# Market Sim

Market Sim is a stock market simulator website that lets users trade stocks with fake currency. Our target audience is anyone interested in the stock market but don’t necessarily have the extra funds. Or people who want to experiment with the stock market but don’t want to gamble their money. Using real time market data from Alpaca API, users will be able to interact with the simulated stock market. They will be able to see their portfolio on the homepage to check whether their shares are in profit or not. Their goal is to maximize their capital. Users would be able to search for tickers and also trade them by buying or selling at market price. The database will keep track of the users accounts, trade history and their current portfolio.


## Setting up your dev environment

1. Before your start setting up the dev environment, make sure you have [Node.js](https://nodejs.org/en/download/) and [PostgreSQL](https://www.postgresql.org/download/) installed.

2. To set up your dev environment, first go into the server directory and install the dependencies:

```
cd server
npm i
```

3. Now go into the client directory and install the dependencies:

```
cd ..
cd client
npm i
```

4. Set up the api config file by going into the `./server/config/` directory and opening up `api_config.json`. Replace the `key_id` and `secret` with your key and secret from [Alpaca](https://app.alpaca.markets/brokerage/dashboard/overview).

### Setting up the database
Next, set up a database with the name `marketsimdb` in PostgresSQL. 

5. `cd` back to the `server` directory. Create the database:
```
createdb -U postgres marketsimdb
```
6. Login to the database you created:
```
psql -U postgres marketsimdb
```
7. Change directories to where the init file is:
```
[Windows]
\! dir
\cd '.\\database'

[Linux]
\! pwd
\cd ./database
```
8. Run this sql file
```
\i init.sql
```
9. Then open up the `./server/config/db_config.json` file and replace the `password` field with your database's password. 

### Running the backend server and client server

10. Inside the server directory, run the server by executing the command:

```
npm start
```

11. Then, in a seperate terminal, go inside the client directory, and start the React app by executing the command:

```
npm start
```

12. Now that the backend and frontend are up, you should be able to visit the website at http://localhost:3000.

Remember that both front end and back end servers have to be running simultaneously for the website to function properly.
