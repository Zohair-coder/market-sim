const express = require("express");
const Delete = require("./deleteClass");
const router = express.Router();
const { verifyToken } = require("../../token/token")

router.post("/", async (req, res) => {
    const verify = await verifyToken(req.headers["x-auth"]);
    if (verify.data.status !== "OK") {
        return res.sendStatus(400);
    }
    let user = verify.data.username;
    const delAccount = new Delete();
    if (!user) {
        return res.status(400).send();
    }
    await delAccount.deleteAccount(user);
    return res.send();
});

module.exports = router;