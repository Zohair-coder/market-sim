const Database = require('../../database/Database');
const db_config = require("../../config/db_config");

class Delete {
    constructor() {
        const database = new Database(db_config);
        this.database = database;
    }

    async fetchData() {
        this.uid = await this.database.getUID(this.username);
    }
    
    async deleteAccount(user) {
        this.username = user;
        await this.fetchData();
        await this.database.deleteAccount(this.uid);
        return;
    }
}
module.exports = Delete;
