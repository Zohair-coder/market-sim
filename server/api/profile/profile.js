const express = require("express")
const router = express.Router();
const Profile = require("./profileClass");
const { verifyToken } = require("../../token/token")

router.get("/", async (req, res) => {
    const verify = await verifyToken(req.headers["x-auth"]);

    if (verify.data.status !== "OK") {
        return res.sendStatus(400);
    }
    let user = verify.data.username;
    const profile = new Profile();
    let balance = await profile.getUserBalance(user);

    if (!balance) {
        return res.status(400).send();
    }

    return res.status(200).json({ balance: balance });
});

module.exports = router;