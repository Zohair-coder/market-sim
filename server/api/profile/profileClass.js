const Database = require('../../database/Database');
const db_config = require("../../config/db_config");

class Profile {
    constructor() {
        const database = new Database(db_config);
        this.database = database;
    }

    async fetchData() {
        // recalls database
        this.balance = await this.database.getUserBalance(this.username);
    }

    async getUserBalance(user) {
        this.username = user;
        await this.fetchData();

        if(this.balance instanceof Error){
            return { error: this.balance }
        }

        return {balance: this.balance}
    }
}

module.exports = Profile;