const Database = require('../../database/Database');
const db_config = require("../../config/db_config");
const bcrypt = require('bcryptjs');

class UpdatePassword {
    constructor() {
        const database = new Database(db_config);
        this.database = database;
    }

    async fetchData() {
        this.password = await this.database.getHashedPassword(this.usersname);
    }

    async checkPassword(user, oldPassword) {
        this.password = await this.database.getHashedPassword(user);
        return bcrypt.compareSync(oldPassword, this.password);
    }

    async updatePassword(user, newPassword) {
        this.password = this.#getHash(newPassword);
        await this.database.updatePassword(user, this.password);
        return;
    }
    
    #getHash(password) {
        const saltRounds = 10;
        return bcrypt.hashSync(password, saltRounds);
    }
}

module.exports = UpdatePassword;