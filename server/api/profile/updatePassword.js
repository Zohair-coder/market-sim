const express = require("express")
const router = express.Router();
const UpdatePassword = require("./updatePasswordClass.js");
const { verifyToken } = require("../../token/token")

router.get("/", async (req, res) => {
    const verify = await verifyToken(req.headers["x-auth"]);
    const password = req.headers['password'];

    if (verify.data.status !== "OK") {
        return res.sendStatus(400);
    }
    let user = verify.data.username;

    if(!user || !password) {
        console.log("no username or password")
        return res.status(400).send();
    }

    const update = new UpdatePassword();
    const check = await update.checkPassword(user, password);
    if (!check) {
        console.log("failed checking")
        return res.status(400).send();
    }

    return res.status(200).json({valid: check});
});

router.post("/", async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    if(!username || !password) {
        if(!username) {
            console.log("no username");
        }
        if(!password) {
            console.log("no password"); 
        }
        return res.status(400).send();
    }
    const update = new UpdatePassword();
    await update.updatePassword(username, password);

    return res.status(200).send();
})

module.exports = router;