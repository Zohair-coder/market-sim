const express = require("express");
const router = express.Router();

router.use("/auth", require("./auth/auth"));
router.use("/stocks", require("./stocks/stocksHandler"))
router.use("/portfolio", require("./portfolio/portfolio"))
router.use("/trade", require("./trade/tradeHandler"))
router.use("/update", require("./profile/updatePassword"))
router.use("/profile", require("./profile/profile"));
router.use("/delete", require("./profile/delete"));
router.use("/watch", require("./watch/watchHandler"))

module.exports = router;