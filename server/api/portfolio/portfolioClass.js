const Database = require('../../database/Database');
const db_config = require("../../config/db_config");

class Portfolio {
    constructor() {
        const database = new Database(db_config);
        this.database = database;
    }

    async fetchData() {
        // recalls database
        this.balance = await this.database.getUserBalance(this.username);
        // console.log(this.balance);
        this.stocks = await this.database.getUserStocks(this.username);
        // console.log(this.stocks);
        this.uid = await this.database.getUID(this.username);
        // console.log(this.uid);
        this.trades = await this.database.getUserTrades(this.uid);
        // console.log(this.trades);
        this.watch = await this.database.getUserWatchList(this.uid);
    }

    getCurrentStockPrice(stock) {
        // TODO: implement getting the stock price
        // Proved to be redundant, now calculating returns and stock price client side.
        return 42.00;
    }
    
    getAllCalculatedReturns() {
        const stocks = this.stocks;
        const  trades = this.trades;
        if (!stocks || !trades) {
            return undefined;
        }

        for (let stock of stocks) {
            let sum = 0;
    
            for (let trade of trades) {
                if (trade.symbol === stock.symbol) {
                    if (trade.type === "buy") {
                        sum += trade.price * trade.amount;
                    } else if (trade.type === "sell") {
                        sum -= trade.price * trade.amount;
                    }
                }
            }
            
            stock['current_price'] = this.getCurrentStockPrice(stock);
            stock['avg_purchase_price'] = sum/stock.amount;
            stock['returns'] = parseFloat((stock.amount * this.getCurrentStockPrice(stock)) - sum);
        }
        return stocks;
    }

    getAllWatchPrices() {
        const watchList = this.watch;
        if (!watchList) {
            return undefined;
        }

        for (let stock of watchList) {
            stock['current_price'] = this.getCurrentStockPrice(stock);
        }
        return watchList;
    }

    async getPortfolioData(user) {
        this.username = user;
        await this.fetchData();

        if (this.balance instanceof Error ||
            this.stocks  instanceof Error ||
            this.trades  instanceof Error ||
            this.watch   instanceof Error) {
            return { error: [this.balance, this.stocks, this.trades, this.watch] }
        }

        this.stocks = this.getAllCalculatedReturns();
        this.watch = this.getAllWatchPrices();
        //console.log({balance: this.balance, stocks: this.stocks, trades: this.trades});
        return {balance: this.balance, stocks: this.stocks, trades: this.trades, watch: this.watch};
    }
}

module.exports = Portfolio;

const portfolio = new Portfolio('dora1');