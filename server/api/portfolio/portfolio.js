const express = require("express");
const router = express.Router();
const Portfolio = require("./portfolioClass");
const { verifyToken } = require("../../token/token")

router.get("/", async (req, res) => {
    let verified;

    try {
        verified = await verifyToken(req.headers['x-auth'])
    } catch(error) {
        return res.sendStatus(401);
    }

    if (verified.data.status !== "OK") {
        return res.sendStatus(401);
    }
    const username = verified.data.username;

    const portfolio = new Portfolio();
    let portfolioData = await portfolio.getPortfolioData(username);

    if (portfolioData.hasOwnProperty("error")) {
        // Avoid repeat error logs
        let errorList = portfolioData.error;
        let prevError = errorList[0];
        console.error(prevError);

        for (let i = 1; i < portfolioData.error.length; i++) {
            if (prevError.code !== errorList[i].code) {
                console.error(errorList[i]);
            }
            prevError = errorList[i];
        }
        return res.status(500).json();
    }

    let balance = portfolioData.balance;
    let stocks = portfolioData.stocks;
    let trades = portfolioData.trades;
    let watch = portfolioData.watch;

    if (!balance || !stocks || !trades || !watch) {
        return res.status(400).send();
    }

    return res.status(200).json({ username: username, balance: balance, stocks: stocks, trades: trades, watch: watch});
});

module.exports = router;