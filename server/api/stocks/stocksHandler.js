const express = require("express");
const router = express.Router();
const Stocks = require("./stocks");

const stocks = new Stocks()


router.get("/symbols", async (req, res) => {

    const token = req.headers["x-auth"];
    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }

    let symbols = await stocks.alpaca.getAllStockSymbols();

    return res.json({
        "status": "OK",
        "data": symbols
    });

});

router.get("/stocks", async (req, res) => {

    const token = req.headers["x-auth"];
    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }

    let stockData = await stocks.alpaca.getAllStocks();

    return res.json({
        "status": "OK",
        "data": stockData
    });

});

router.post("/subscribe", async (req, res) => {

    const token = req.headers["x-auth"];

    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }

    const symbol = req.headers["symbol"];

    if (symbol === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing symbol header"
        });
    }

    let timeout = undefined;
    if (req.headers.hasOwnProperty("timeout")) {
        timeout = req.headers.timeout
    }



    return res.json(await stocks.subscribe(symbol, timeout))


})

router.post("/unsubscribe", async (req, res) => {

    const token = req.headers["x-auth"];

    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }

    const symbol = req.headers["symbol"];

    if (symbol === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing symbol header"
        });
    }

    return res.json(await stocks.unsubscribe(symbol))


})

router.post("/resubscribe", async (req, res) => {

    const token = req.headers["x-auth"];

    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }

    const symbol = req.headers["symbol"];

    if (symbol === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing symbol header"
        });
    }

    let timeout = undefined;
    if (req.headers.hasOwnProperty("timeout")) {
        timeout = req.headers.timeout
    }

    return res.json(await stocks.resubscribe(symbol, timeout))


})

router.get("/quote", async (req, res) => {

    const token = req.headers["x-auth"];

    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }

    const symbol = req.headers["symbol"];

    if (symbol === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing symbol header"
        });
    }

    return res.json(await stocks.getQuote(symbol))


})

router.get("/subscribed", async (req, res) => {

    const token = req.headers["x-auth"];

    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }


    return res.json(await stocks.getSubscribedStocks())


})

router.get("/marketdata", async (req, res) => {

    const token = req.headers["x-auth"];

    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }

    return res.json(stocks.getMarketData())

})

module.exports = router;