const express = require("express");
const router = express.Router();
const alpaca = require("../../alpaca/socketLauncher");



class Stocks {
    constructor(){
        this.alpaca = alpaca
        this.header ={
            "status": "OK",
            "data": null,
            "error": null
        }
        
    }


    async subscribe(symbol, timeout=300000){

        if(!await this.validateSymbol(symbol)){
            return {
                "status": "NOT_OK",
                "error": "Symbol Does Not Exist"
            }
        }

        if(!await this.alpaca.subscribeQuotes([symbol], timeout)){
            return {
                "status": "NOT_OK",
                "error": "Error subscribing to quote"
            }
        }
        
        return this.header

    }

    async unsubscribe(symbol){
        if(!await this.validateSymbol(symbol)){
            return {
                "status": "NOT_OK",
                "error": "Symbol Does Not Exist"
            }
        }

        if(!await this.alpaca.unSubscribeQuotes([symbol])){
            return {
                "status": "NOT_OK",
                "error": "Error subscribing to quote"
            }
        }

        return this.header


    }

    async resubscribe(symbol, timeout =300000){
        if(!await this.validateSymbol(symbol)){
            return {
                "status": "NOT_OK",
                "error": "Symbol Does Not Exist"
            }
        }

        if(!await this.alpaca.resetSubscriptionQuoteTimeout(symbol, timeout)){
            return {
                "status": "NOT_OK",
                "error": "Error subscribing to quote"
            }
        }

        return this.header

    }

    async getQuote(symbol){
        if (!await this.validateSymbol(symbol)){
            return {
                "status": "NOT_OK",
                "error": "Symbol Does Not Exist"
            }
        }
        
        let quote = await this.alpaca.getStockQuote(symbol)
        
        if (quote === null){
            return {
                "status": "NOT_OK",
                "error": "Error Retreiving Quote"
            }
        }
     
        let header = this.header
        header["data"] = quote
        return header

    }


    async validateSymbol(symbol){
        let symbols = await this.alpaca.getAllStockSymbols()
        if(!symbols.includes(symbol)){
            return false
        }
        return true
    }

    getSubscribedStocks(){
        let symbols = this.alpaca.getSubscribedQuotes()
        let header = this.header
        header["data"] = symbols
        return header
    }

    getMarketData(){
        let data = this.alpaca.getMarketData()
        let header = this.header
        header["data"] = data
        return header
    }

} 

module.exports = Stocks;