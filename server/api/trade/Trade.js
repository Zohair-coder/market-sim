
const Database = require("../../database/database");
const db_config = require("../../config/db_config");
const trade_config = require("../../config/trade_config");
const Alpaca = require("../../alpaca/Alpaca")

class Trade {
    constructor(){
        this.database = new Database(db_config);
        this.alpaca = new Alpaca(false)
        this.trade_config = trade_config
    }

    async validateUser(user){
        if (user.length > 20){
            return false
        }
        if (await this.database.getUID(user) == null) {
            return false
        }
        return true
    }

    async validateStock(stock){
        let symbols = await this.alpaca.getAllStockSymbols()
        if(!symbols.includes(stock)){
            return false
        }
        return true
    }

    async validatePrice(stock, price) {
        
        let curMarketPrice = await this.alpaca.getStockQuote(stock);

        //check price is within the percent difference limit of market value
        if (Math.abs(1-(curMarketPrice/price)) > this.trade_config["marketDifferenceLimit"]){
            return false
        }
        return true
    }

    validateTypes(user, stock, amount, price) {
        return (this.isString(user) && this.isString(stock) && this.isFloat(amount) && this.isFloat(price))
    }

    isInt(integer) {
        return Number.isInteger(integer)
    }

    isFloat(float) {
        return (typeof float  == 'number' && !isNaN(float))
    }

    isString(string){
        return (typeof string =="string")
    }



}
module.exports = Trade;

/*
async function testing(){
    const trade = new Trade()
    console.log(await trade.validateStock('AAPL'))
}

testing()
*/