
const Trade = require('./Trade')

class Buy extends Trade {
    constructor() {
        super();
    }

    /*
    * Input: user (string: username), stock (string: stock symbol), amount (float: number stocks), price (float: price per 1 stock)
    * 
    * Description: checks to see if symbol is avlible on alpaca api
    * 
    */
    async buy (user, stock, amount, price) {


        let error = await this.buyValidator(user, stock, amount, price)

        if (error !== null) {
            return {
                "status": "NOT_OK",
                "error": error
            };
        }

        error = await this.buyProcessor(user, stock, amount, price)

        if (error !== null) {
            return {
                "status": "NOT_OK",
                "error": error
            };
        }

        return {"status": "OK",
                "error": error}

    }


    async buyProcessor (user, stock, amount, price) {
        let cost = amount * price;
        let uid = await this.database.getUID(user)

        //update trades history
        if (!await this.database.addTrade(uid, 'buy', stock, price, amount)){
            return "Database Error - Add Trade"
        }

        let curStocks = await this.database.getUserStocks(user)
        
        //update stock amount in ownedStock if user already owns some of this stock
        let exists = 0;
        for (let ownedStock of curStocks) {
            if (ownedStock.symbol == stock) {
                let newAmount = amount + parseFloat(ownedStock.amount)
                if (!await this.database.setStockAmount(ownedStock.sid, newAmount)){
                    return "Database Error - Set Stock"
                }
                exists = 1;
                break
            }
        }

        //create new stock in ownedStock if user does not own this stock yet
        if (!exists) {
            if (!await this.database.addStock(uid, stock, amount)){
                return "Database Error - add Stock"
            }
        }

        //update the users balance to reflect purchase
        let curBalance = await this.database.getUserBalance(user)
        let newBalance = curBalance - cost

        if (!await this.database.setUserBalance(user, newBalance)){
            return "Database Error - Set User Balance"
        }

        return null


    }


    async buyValidator(user, stock, amount, price) {

        if (!this.validateTypes(user, stock, amount, price)){
            return "Invalid Input"
        }

        if (!await this.validateUser(user)) {
            return "Invalid Username"
        }

        if(!await this.validateStock(stock)){
            return "This Stock is Not Avalible to Purchase"
        }

        if (!this.validateAmount(amount)){
            return "Invalid Amount"
        }

        if (!await this.validatePrice(stock, price)){
            return "Invalid Price"
        }

        if (!await this.validateBuyPower(user, amount, price)){
            return "Insufficient Funds"
        }
        return null

    }


    async validateBuyPower(user, amount, price){
        if (await this.database.getUserBalance(user) < (amount * price)){
            return false
        }
        return true
    }

    validateAmount(amount){
        if (amount < this.trade_config["buy"]["minAmount"] ||
             amount > this.trade_config["buy"]["maxAmount"]){
                return false
            }
        return true
    }


}
module.exports = Buy;

/*
//testing
async function testing() {
    const buy = new Buy()

    //console.log(await buy.validateBuyPower("dora1", 1.32, 323.32))
    console.log(await buy.buy("dora1",  'GOOGL', 2.32, 99999999.15))
}

testing()
*/