const express = require("express");
const router = express.Router();
const { isValidToken, getUsername } = require("../auth/verify")
const Buy = require("./Buy");
const Sell = require("./Sell");


const buy = new Buy();
const sell = new Sell();


router.post("/buy", async (req, res) => {

    const token = req.headers["x-auth"];
    const stock = req.headers["stock"];
    const amount = req.headers["amount"];
    const price = req.headers["price"];
    let user;

    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }

    if (isValidToken(token)){
        user = getUsername(token)
    } else {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Invalid token"
        });
    }

    return res.json(await buy.buy(user, stock, parseFloat(amount), parseFloat(price)));

});

router.post("/sell", async (req, res) => {

    const token = req.headers["x-auth"];
    const stock = req.headers["stock"];
    const amount = req.headers["amount"];
    const price = req.headers["price"];
    let user;

    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }

    if (isValidToken(token)){
        user = getUsername(token)
    } else {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Invalid token"
        });
    }

    return res.json(await sell.sell(user, stock, parseFloat(amount), parseFloat(price)));


});

module.exports = router;