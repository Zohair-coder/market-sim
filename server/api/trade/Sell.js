
const Trade = require('./Trade.js')

class Sell extends Trade {
    constructor() {
        super();
    }


    /*
    * Input: user (string: username), stock (string: stock symbol), amount (float: number stocks), price (float: price per 1 stock)
    * 
    * Description: checks to see if symbol is avlible on alpaca api
    * 
    */
    async sell (user, stock, amount, price) {


        let error = await this.sellValidator(user, stock, amount, price)

        if (error !== null) {
            return {
                "status": "NOT_OK",
                "error": error
            };
        }

        error = await this.sellProcessor(user, stock, amount, price)

        if (error !== null) {
            return {
                "status": "NOT_OK",
                "error": error
            };
        }

        return {"status": "OK",
                "error": error}

    }


    async sellProcessor (user, stock, amount, price) {
        let cost = amount * price;
        let uid = await this.database.getUID(user)

        //update trades history
        if (!await this.database.addTrade(uid, 'sell', stock, price, amount)){
            return "Database Error - Add Trade"
        }

        //get curent stock amount
        let curStocks = await this.database.getUserStocks(user)
        for (let ownedStock of curStocks) {
            if (ownedStock.symbol == stock) {

                let curAmount = parseFloat(ownedStock.amount)

                //set the new amount
                let newAmount = curAmount - amount
                if (newAmount == 0){
                    if (!await this.database.removeStock(ownedStock.sid)){
                        return "Database Error - remove Stock"
                    }
                } else {
                    if (!await this.database.setStockAmount(ownedStock.sid, newAmount)){
                        return "Database Error - Set Stock"
                    }
                    break;
                }
            }
        }





        //update the users balance to reflect purchase
        let curBalance = await this.database.getUserBalance(user)
        let newBalance = curBalance + cost

        if (!await this.database.setUserBalance(user, newBalance)){
            return "Database Error - Set User Balance"
        }

        return null


    }


    async sellValidator(user, stock, amount, price) {

        if (!this.validateTypes(user, stock, amount, price)){
            return "Invalid Input"
        }

        if (!await this.validateUser(user)) {
            return "Invalid Username"
        }

        if (!this.validateAmount(amount)){
            return "Invalid Amount"
        }

        if(!await this.validateStock(stock)){
            return "This Stock is Not Avalible to Purchase"
        }

        if (!await this.validatePrice(stock, price)){
            return "Invalid Price"
        }

        if (!await this.validateSellPower(user, stock, amount)){
            return "Insufficient Stocks"
        }
        return null

    }


    async validateSellPower(user, stock, amount){
        
        let stocks = await this.database.getUserStocks(user)

        for (let ownedStock of stocks) {
            if (ownedStock.symbol == stock && parseFloat(ownedStock.amount) >= amount) {
                return true
            }
        }

        return false

    }

    validateAmount(amount){
        if (amount < this.trade_config["sell"]["minAmount"] ||
             amount > this.trade_config["sell"]["maxAmount"]){
                return false
            }
        return true
    }

}
module.exports = Sell;
/*
//testing
async function testing() {
    const sell = new Sell()

    //console.log(await buy.validateBuyPower("dora1", 1.32, 323.32))
    console.log(await sell.sell("dora1",  'AAPL', 1.46, 154.15))
}

testing()
*/