const express = require("express");
const router = express.Router();
const Register = require("./register");
const Login = require("./login");
const verify = require("./verify");

router.post("/register", async (req, res) => {
    let username = req.body.username;
    let password = req.body.password;

    const register = new Register();

    let errors = await register.validateRegistration(username, password);

    if (errors.length > 0) {
        return res.status(400).json(errors);
    } else {
        let db_errors = await register.register(username, password);
        if (db_errors !== null) {
            return res.status(400).json([db_errors.message]);
        }
    }

    return res.send();
});

router.post("/login", async (req, res) => {
    let username = req.body.username;
    let password = req.body.password;

    let login = new Login();

    let error = await login.validateLogin(username, password);

    if (error !== null) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": error
        });
    }

    let token = login.login(username);

    return res.json({
        "status": "OK",
        "token": token
    });

})

router.get("/verify", async (req, res) => {
    const token = req.headers["x-auth"];

    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }

    if (!verify.isValidToken(token)) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Invalid token"
        });
    }

    const username = verify.getUsername(token);

    return res.json({
        "status": "OK",
        "username": username
    });
});



module.exports = router;