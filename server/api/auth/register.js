const bcrypt = require('bcryptjs');
const Database = require("../../database/database");
const db_config = require("../../config/db_config");

class Register {
    constructor() {
        this.db = new Database(db_config);
    }

    async validateRegistration(username, password) {
        let errors = [];

        errors = errors.concat(await this.validateUsername(username));
        errors = errors.concat(this.validatePassword(password));

        return errors;
    }

    async validateUsername(username) {
        let errors = [];

        if (username === undefined) {
            errors.push("Username is required");
        }

        if (username.length < 3) {
            errors.push("Username must be at least 3 characters long");
        }

        if (username.length > 20) {
            errors.push("Username must be less than 20 characters long");
        }

        if (username.includes(" ")) {
            errors.push("Username cannot contain spaces");
        }

        if (await this.db.doesUserExist(username)) {
            errors.push("Username is already taken");
        }

        return errors;
    }

    validatePassword(password) {
        let errors = [];

        if (password === undefined) {
            errors.push("Password is required");
            return errors;
        }

        if (password.length < 8) {
            errors.push("Password must be at least 8 characters long");
        }

        return errors;
    }

    register(user, password) {
        const starting_balance = 10000;
        const hash = this.#getHash(password);
        return this.db.register(user, hash, starting_balance);
    }


    #getHash(password) {
        const saltRounds = 10;
        return bcrypt.hashSync(password, saltRounds);
    }
}

module.exports = Register;