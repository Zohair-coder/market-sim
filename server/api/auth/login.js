const bcrypt = require('bcryptjs');
const Database = require("../../database/database");
const db_config = require("../../config/db_config");
const jwt = require("jwt-simple");
const auth_config = require("../../config/auth_config");


class Login {
    constructor() {
        this.db = new Database(db_config);
        this.config = auth_config;
    }

    async validateLogin(username, password) {
        let hashed_password = await this.db.getHashedPassword(username);

        if (hashed_password === null) {
            return "Invalid username or password";
        }

        if (username === undefined) {
            return "Username is required";
        }

        if (password === undefined) {
            return "Password is required";
        }

        if (!this.db.doesUserExist(username)) {
            return "Invalid username or password";
        }

        if (!bcrypt.compareSync(password, hashed_password)) {
            return "Invalid username or password";
        }

        return null;
    }

    login(username) {
        const secret = this.config.jwt_secret;
        return jwt.encode({ username: username }, secret);
    }
}
module.exports = Login;