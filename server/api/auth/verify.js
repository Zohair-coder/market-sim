const jwt = require("jwt-simple");
const secret = require("../../config/auth_config").jwt_secret;

function isValidToken(token) {
    try {
        jwt.decode(token, secret);
        return true;
    } catch (err) {
        return false;
    }
}

function getUsername(token) {
    try {
        const decoded = jwt.decode(token, secret);
        return decoded.username;
    } catch (err) {
        return null;
    }
}

module.exports = { isValidToken, getUsername };