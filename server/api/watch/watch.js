
const Database = require("../../database/database");
const db_config = require("../../config/db_config");
const Alpaca = require("../../alpaca/Alpaca")


class Watch {
    constructor(){
        this.database = new Database(db_config);
        this.alpaca = new Alpaca(false)
    }

    async add(username, stock){

        let error = await this.#watchInputValidator(username, stock)

        if (error !== null) {
            return {
                "status": "NOT_OK",
                "error": error
            };
        }

        let uid = await this.database.getUID(username);

        if (!await this.database.addWatch(uid, stock)){
            error = "Database error"
        }

        if (error !== null) {
            return {
                "status": "NOT_OK",
                "error": error
            };
        }

        return {"status": "OK",
                "error": error}

    }

    async remove(username, stock){
        let error = await this.#watchInputValidator(username, stock)

        if (error !== null) {
            return {
                "status": "NOT_OK",
                "error": error
            };
        }

        let uid = await this.database.getUID(username);

        if (!await this.database.removeWatch(uid, stock)){
            error = "Database error"
        }

        if (error !== null) {
            return {
                "status": "NOT_OK",
                "error": error
            };
        }

        return {"status": "OK",
                "error": error}

    }

    //returns as an array of objects
    async get(username){
        let error = null;
        if (!await this.#validateUser(username)){
            error = "Invalid Username"
        }

        if (error !== null) {
            return {
                "status": "NOT_OK",
                "error": error
            };
        }

        let uid = await this.database.getUID(username);

        let data =  await this.database.getUserWatchList(uid)


        return {"status": "OK",
                "error": error,
                "data": data}

    }

    async #watchInputValidator(username, stock) {

        if (!this.#validateStock(stock)){
            return "Invalid Input"
        }
        if (!await this.#validateUser(username)) {
            return "Invalid Username"
        }

        return null
    }

    async #validateUser(user){
        if (user.length > 20){
            return false
        }
        if (await this.database.getUID(user) == null) {
            return false
        }
        return true
    }

    async #validateStock(stock){
        let symbols = await this.alpaca.getAllStockSymbols()
        if(!symbols.includes(stock)){
            return false
        }
        return true
    }


}
module.exports = Watch;
/*
module.exports = Watch;


//testing
async function testing() {
    const watch = new Watch()

    //console.log(await buy.validateBuyPower("dora1", 1.32, 323.32))
    console.log(await watch.remove("dora1", 'AAPL'))
    console.log(await watch.add("dora1", 'AAPL'))
    console.log(await watch.get("dora1"))
}

testing()
*/