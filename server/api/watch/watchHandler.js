const express = require("express");
const router = express.Router();
const { isValidToken, getUsername } = require("../auth/verify")
const Watch = require("./Watch");

const watch = new Watch();


router.post("/add", async (req, res) => {

    const token = req.headers["x-auth"];
    const stock = req.headers["stock"];
    let user;

    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }

    if (isValidToken(token)){
        user = getUsername(token)
    } else {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Invalid token"
        });
    }

    return res.json(await watch.add(user, stock));

});

router.post("/remove", async (req, res) => {

    const token = req.headers["x-auth"];
    const stock = req.headers["stock"];
    let user;

    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }

    if (isValidToken(token)){
        user = getUsername(token)
    } else {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Invalid token"
        });
    }

    return res.json(await watch.remove(user, stock));

});

router.get("/get", async (req, res) => {

    const token = req.headers["x-auth"];
    let user;

    if (token === undefined) {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Missing X-Auth header"
        });
    }

    if (isValidToken(token)){
        user = getUsername(token)
    } else {
        return res.status(401).json({
            "status": "NOT_OK",
            "error": "Invalid token"
        });
    }

    return res.json(await watch.get(user));

});



module.exports = router;