const { Pool } = require('pg');

class Database {

    constructor(db_config) {

        this.pool = new Pool(db_config);
    }


    getUserBalance(user) {

        let query;
        if (Number.isInteger(user)) {
            query = "SELECT balance FROM users WHERE uid = $1;"
        } else {
            query = "SELECT balance FROM users WHERE username = $1;"
        }

        return this.pool.query(query, [user])
            .then(function (response) {
                if (response.rows.length === 0 || !response.rows[0].hasOwnProperty('balance')) {
                    return null
                } else {
                    return parseFloat(response.rows[0].balance);
                }
            })
            .catch(function (error) {
                return error;
            });
    }

    getUserStocks(user) {

        let query;
        if (Number.isInteger(user)) {
            query = "SELECT * FROM ownedStock WHERE uid = $1;"
        } else {
            query = "SELECT * FROM ownedStock WHERE uid in (SELECT uid FROM users WHERE username = $1);"
        }

        let self = this;
        return this.pool.query(query, [user])
            .then(function (response) {
                if (response.rows.length === 0) {
                    return []
                } else {
                    return self.#removeTrailingWhiteSpaceFromObjectArray(response.rows)
                }
            })
            .catch(function (error) {
                return error;
            });
    }

    addTrade(uid, type, symbol, price, amount) {

        return this.pool.query("INSERT INTO trades(uid, type, symbol, price, amount, time) VALUES ($1, $2, $3, $4, $5, NOW());", [uid, type, symbol, price, amount])
            .then(function (response) {
                return true
            })
            .catch(function (error) {
                return false;
            });
    }

    getUserTrades(uid) {

        let self = this
        return this.pool.query("SELECT * FROM trades WHERE uid=$1;", [uid])
            .then(function (response) {
                if (response.rows.length === 0) {
                    return []
                } else {
                    return self.#removeTrailingWhiteSpaceFromObjectArray(response.rows)
                }
            })
            .catch(function (error) {
                return error;
            });
    }

    getUserWatchList(uid) {

        let self = this
        return this.pool.query("SELECT * FROM watch WHERE uid=$1;", [uid])
            .then(function (response) {
                if (response.rows.length === 0) {
                    return []
                } else { 
                    return self.#removeTrailingWhiteSpaceFromObjectArray(response.rows)
                }
            })
            .catch(function (error) {
                return error;
            });
    }

    setUserBalance(user, balance) {

        let query;
        if (Number.isInteger(user)) {
            query = "UPDATE users SET balance=$1 WHERE uid=$2;"
        } else {
            query = "UPDATE users SET balance=$1 WHERE username=$2;"
        }

        return this.pool.query(query, [balance, user])
            .then(function (response) {
                return true
            })
            .catch(function (error) {
                return false;
            });
    }

    addStock(uid, symbol, amount) {

        return this.pool.query("INSERT INTO ownedStock(uid, symbol, amount) VALUES ($1, $2, $3);", [uid, symbol, amount])
            .then(function (response) {
                return true
            })
            .catch(function (error) {
                return false;
            });
    }

    removeStock(sid) {

        return this.pool.query("DELETE FROM ownedStock WHERE sid=$1;", [sid])
            .then(function (response) {
                return true
            })
            .catch(function (error) {
                return false;
            });
    }

    addWatch(uid, symbol) {

        return this.pool.query("INSERT INTO watch(uid, symbol) VALUES ($1, $2);", [uid, symbol])
            .then(function (response) {
                return true
            })
            .catch(function (error) {
                return false;
            });
    }

    removeWatch(uid, symbol) {

        return this.pool.query("DELETE FROM watch WHERE uid=$1 AND symbol=$2;", [uid, symbol])
            .then(function (response) {
                return true
            })
            .catch(function (error) {
                return false;
            });
    }

    setStockAmount(sid, amount) {

        return this.pool.query("UPDATE ownedStock SET amount=$1 WHERE sid=$2;", [amount, sid])
            .then(function (response) {
                return true
            })
            .catch(function (error) {
                return false;
            });

    }

    getStockAmount(sid) {

        return this.pool.query("SELECT * FROM ownedStock WHERE sid=$1;", [sid])
            .then(function (response) {
                if (response.rows.length === 0 || !response.rows[0].hasOwnProperty('amount')) {
                    return null
                } else {
                    return parseFloat(response.rows[0].amount)
                }
            })
            .catch(function (error) {
                return null;
            });
    }

    getSID(uid, symbol) {

        return this.pool.query("SELECT sid FROM ownedStock WHERE (uid=$1) AND (symbol=$2);", [uid, symbol])
            .then(function (response) {
                if (response.rows.length === 0 || !response.rows[0].hasOwnProperty('sid')) {
                    return null
                } else {
                    return response.rows[0]['sid']
                }
            })
            .catch(function (error) {
                return null;
            });
    }

    getUID(username) {

        return this.pool.query("SELECT uid FROM users WHERE (username=$1);", [username])
            .then(function (response) {
                if (response.rows.length === 0 || !response.rows[0].hasOwnProperty('uid')) {
                    return null;
                } else {
                    return response.rows[0]['uid']
                }
            })
            .catch(function (error) {
                return null;
            });
    }

    register(user, hashed_password, balance) {
        return this.pool.query("INSERT INTO users (username, password, balance) VALUES ($1, $2, $3);", [user, hashed_password, balance])
            .then(function (response) {
                return null;
            })
            .catch(function (error) {
                return error;
            });
    }

    doesUserExist(user) {
        return this.pool.query("SELECT * FROM users WHERE username = $1;", [user])
            .then(function (response) {
                return response.rows.length !== 0;
            })
            .catch(function (error) {
                return false;
            });
    }

    getHashedPassword(user) {
        return this.pool.query("SELECT password FROM users WHERE username = $1;", [user])
            .then(function (response) {
                if (response.rows.length === 0 || !response.rows[0].hasOwnProperty('password')) {
                    return null;
                } else {
                    return response.rows[0]['password']
                }
            })
            .catch(function (error) {
                return null;
            });
    }

    async deleteAccount(uid) {
        await this.pool.query("DELETE FROM ownedstock WHERE uid = $1;", [uid])
        await this.pool.query("DELETE FROM trades WHERE uid = $1;", [uid])
        await this.pool.query("DELETE FROM watch WHERE uid = $1;", [uid])
        await this.pool.query("DELETE FROM users WHERE uid = $1;", [uid])
    }

    updatePassword(user, hashed_password) {
        return this.pool.query("UPDATE users SET password=$1 WHERE username = $2", [hashed_password, user])
            .then(function (reponse){
                return true;
            })
            .catch(function (error) {
                return false;
            })
    }

    #removeTrailingWhiteSpaceFromObject = function (resObj) {

        for (let key in resObj) {
            let value = resObj[key]
            if (typeof value === 'string') {
                resObj[key] = value.trim()
            }
        }

        return resObj
    }

    #removeTrailingWhiteSpaceFromObjectArray(arry) {
        let i = 0
        for (let resObj of arry) {
            arry[i] = this.#removeTrailingWhiteSpaceFromObject(resObj)
            i += 1
        }
        return arry
    }
}
module.exports = Database;