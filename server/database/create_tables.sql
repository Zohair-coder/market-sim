
DROP TABLE users CASCADE;
DROP TABLE trades CASCADE;
DROP TABLE ownedStock CASCADE;
DROP TABLE watch CASCADE;


CREATE TABLE users
(
	uid          	SERIAL NOT NULL,
	username        char(20) NOT NULL UNIQUE,
	password 		char(60) NOT NULL,
	balance         decimal NOT NULL,
	PRIMARY KEY ( uid )
);


CREATE TABLE trades
(
	tid      		SERIAL NOT NULL,
	uid  			integer NOT NULL,
	type			char(10) NOT NULL,
	symbol  		char(10) NOT NULL,
	price   		decimal NOT NULL,
	amount  		decimal NOT NULL,
	time    		timestamp NOT NULL,
	PRIMARY KEY ( tid ),
	FOREIGN KEY ( uid ) REFERENCES users ( uid )
);

CREATE TABLE ownedStock
(	
	sid 		SERIAL NOT NULL,
	uid      	integer NOT NULL,
	symbol      char(10) NOT NULL,
	amount 		decimal NOT NULL,
	PRIMARY KEY ( sid ),
	FOREIGN KEY ( uid ) REFERENCES users ( uid ),
	UNIQUE(uid, symbol)
);

CREATE TABLE watch
(
	wid			SERIAL NOT NULL,
	uid      	integer NOT NULL,
	symbol      char(10) NOT NULL,
	PRIMARY KEY ( wid ),
	FOREIGN KEY ( uid ) REFERENCES users ( uid ),
	UNIQUE(uid, symbol)
);
