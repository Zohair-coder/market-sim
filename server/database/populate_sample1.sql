

/*input users */

/*password: pass1*/
INSERT INTO users(username, password, balance)
values ('dora1', '$2b$10$049L.jDKr9w/JGtiAeXG4eaH4SLVJOfgaeidfoBuhjzd7kXfIU3au', 10000);
/*password: pass2*/
INSERT INTO users(username, password, balance)
values ('dora2', '$2b$10$dQa/aRvL9WKGrHR6.Yj1weVIOm7aKia6mH6lcjOmGGectAt0XOkiO', 10000);
/*password: pass3*/
INSERT INTO users(username, password, balance)
values ('dora3', '$2b$10$nJ49zveXhwFzGfOAP68p1eqRvh/Vsnlp/auCrR/uJit2wC6.LmF.W', 10000);
/*password: pass4*/
INSERT INTO users(username, password, balance)
values ('dora4', '$2b$10$0OchdEwUn5k72quEZoym7u7cz5XnHb5S2m1UkMgw0tnG86UW.Ku7m', 10000);
/*password: pass5*/
INSERT INTO users(username, password, balance) 
values ('dora5', '$2b$10$fbwkucCRtDJ3Bm66YDVJ2ek.ZEsqRuhVujmUcKDF1ciZ8TcXUGp7K', 10000);




/*input trade history*/
INSERT INTO trades(uid, type, symbol, price, amount, time) 
values (1, 'buy', 'AAPL', 172.2, 3.1, NOW());

INSERT INTO trades(uid, type, symbol, price, amount, time) 
values (2, 'buy', 'GOOGL', 2551.78, 2.72, NOW());

INSERT INTO trades(uid, type, symbol, price, amount, time)
values (1, 'sell', 'AAPL', 175.2, 1.0, NOW());

INSERT INTO trades(uid, type, symbol, price, amount, time)
values (3, 'buy', 'AAPL', 172.45, 7.1, NOW());

INSERT INTO trades(uid, type, symbol, price, amount, time)
values (2, 'buy', 'NFLX', 367.83, 9.32, NOW());

INSERT INTO trades(uid, type, symbol, price, amount, time)
values (4, 'buy', 'NFLX', 280.33, 6.69, NOW());

INSERT INTO trades(uid, type, symbol, price, amount, time) 
values (3, 'sell', 'AAPL', 171.36, 5.23, NOW());


/* Current stocks held */
INSERT INTO ownedStock(uid, symbol, amount)
values (1, 'AAPL', 2.1);

INSERT INTO ownedStock(uid, symbol, amount)
values (2, 'GOOGL', 2.72);

INSERT INTO ownedStock(uid, symbol, amount)
values (2, 'NFLX', 9.32);

INSERT INTO ownedStock(uid, symbol, amount)
values (3, 'AAPL', 1.87);

INSERT INTO ownedStock(uid, symbol, amount)
values (3, 'NFLX', 6.69);


/* Current watched stocks */
INSERT INTO watch(uid, symbol)
values (1, 'AMD');

INSERT INTO watch(uid, symbol)
values (2, 'NFLX');

INSERT INTO watch(uid, symbol)
values (2, 'AAPL');

INSERT INTO watch(uid, symbol)
values (3, 'GOOGL');

INSERT INTO watch(uid, symbol)
values (3, 'GME');

INSERT INTO watch(uid, symbol)
values (4, 'NVDA');

INSERT INTO watch(uid, symbol)
values (4, 'AAPL');

INSERT INTO watch(uid, symbol)
values (5, 'GOOGL');

INSERT INTO watch(uid, symbol)
values (5, 'AMC');