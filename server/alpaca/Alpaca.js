
const fetch = require('node-fetch');

const api_config = require("../config/api_config");

let WebSocket = require('ws')
const AlpacaSDK = require('@alpacahq/alpaca-trade-api')


class Alpaca extends AlpacaSDK {
    #api_config;
    #subscribedQuotes;

    constructor(initSocket = false) {
        super({
            keyId: api_config.key_id,
            secretKey: api_config.secret,
            paper: false
        })

        this.#api_config = api_config;
        this.baseURL = "https://data.alpaca.markets/v2/stocks/"
        this.marketData = {};
        this.#subscribedQuotes = {}
        this.connected = false

        if (initSocket) {
            this.#initSocket()
        }

    }

    /*
    * Input: timeout
    * 
    * Description: Launch alpaca websocket and handle responses from it
    */
    #initSocket() {
        this.socket = new WebSocket(`wss://stream.data.alpaca.markets/v2/sip`);

        let self = this
        self.socket.addEventListener('open', function (event) {
            console.log("Alpaca Web Socket Connected");
            let auth_info = { "action": "auth", "key": api_config.key_id, "secret": api_config.secret };
            self.socket.send(JSON.stringify(auth_info));
        });

        //Store stonk data into marketData
        self.socket.addEventListener('message', function (event) {
            let data = JSON.parse(event.data);
            if (data[0].S) {
                self.marketData[data[0].S] = data[0].ap;
                //console.log(this.marketData)
            } else {
                console.log(event.data)
                if (event.data = [{ "T": "success", "msg": "authenticated" }]) {
                    this.connected = true
                }
            }
        });
    }

    async closeSocket() {
        await this.#awaitSocketConnection();
        this.socket.close()
    }
    /*
    * Input: timeout
    * 
    * Description: Waits until the websocket is successfully connected
    */
    async #awaitSocketConnection(timeout = 20000) {

        if (this.connected) {
            return
        }
        let start = Date.now();
        let socket = this.socket

        //https://codepen.io/eanbowman/pen/jxqKjJ
        await new Promise(waitForSocketConnection);
        function waitForSocketConnection(resolve, reject) {
            if (socket.readyState === WebSocket.OPEN)
                resolve();
            else if (timeout && (Date.now() - start) >= timeout)
                reject(new Error("timeout"));
            else
                setTimeout(waitForSocketConnection.bind(this, resolve, reject), 30);
        }
    }

    /*
    * Input: array<symbol> timeout - auto unsub after this long (default 5min)
    * 
    * Description: Adds symbols to websocket Quotes list
    */
    async subscribeQuotes(symbols, timeout = 300000) {

        await this.#awaitSocketConnection();

        //set initial value
        for (let symbol of symbols) {
            this.marketData[symbol] = await this.getStockQuote(symbol)
        }

        for (let symbol of symbols) {
            if (!this.#subscribedQuotes.hasOwnProperty(symbol)) {

                let quoteTimeout = setTimeout(this.unSubscribeQuotes.bind(this), timeout, [symbol]);
                this.#subscribedQuotes[symbol] = quoteTimeout;

                let subscription = { "action": "subscribe", "quotes": [symbol] };
                this.socket.send(JSON.stringify(subscription));

                //console.log(this.#subscribedQuotes)

            }
        }

        return true
    }


    /*
    * Input: array<symbol>
    * 
    * Description: Removes symbols from websocket Quotes list
    */
    async unSubscribeQuotes(symbols) {

        //console.log('unsub')
        await this.#awaitSocketConnection();

        for (let symbol of symbols) {
            if (this.#subscribedQuotes.hasOwnProperty(symbol)) {
                let subscription = { "action": "unsubscribe", "quotes": [symbol] };
                this.socket.send(JSON.stringify(subscription));

                clearTimeout(this.#subscribedQuotes[symbol])
                delete this.#subscribedQuotes[symbol]

                if (this.marketData.hasOwnProperty(symbol)) {
                    delete this.marketData[symbol]
                }
                //console.log(this.#subscribedQuotes)

            }
        }
        return true

    }

    /*
    * Input: symbol timeout - auto unsub after this long (default 5min)
    * 
    * Description: resets subscription timer, subscribes if there is no timer
    */
    async resetSubscriptionQuoteTimeout(symbol, timeout = 300000) {

        if (this.#subscribedQuotes.hasOwnProperty(symbol)) {

            let oldTimeout = this.#subscribedQuotes[symbol]
            clearTimeout(oldTimeout)

            let newTimeout = setTimeout(this.unSubscribeQuotes.bind(this), timeout, [symbol])
            this.#subscribedQuotes[symbol] = newTimeout
        } else {
            await this.subscribeQuotes([symbol], timeout)
        }
        return true

    }


    /*
    * 
    * Description: get a list of all avalible stocks
    * 
    *  return example:
    * [{
    *    id: 'bd1167bd-e701-48f3-bf16-16a5203cc876',
    *    class: 'us_equity',
    *    exchange: 'NASDAQ',
    *    symbol: 'BWAQU',
    *    name: 'Blue World Acquisition Corporation Unit',
    *    tradable: true,
    *    marginable: true,
    *    shortable: true,
    *    easy_to_borrow: true,
    *    fractionable: false
    *  }]
    */

    async getAllStocks() {

        const activeAssets = await this.getAssets({ status: 'active' })

        // Filter the assets down to just those tradable on Alpaca
        //const tradeableAssets = activeAssets.filter(asset => asset.tradable == true)

        return activeAssets

    }

    /*
    * 
    * Description: Removes symbols from websocket Quotes list
    * 
    * return: array[stock symbols]
    */
    async getAllStockSymbols() {

        const activeAssets = await this.getAssets({ status: 'active' })

        // Filter the assets down to just those tradable on Alpaca
        //const tradeableAssets = activeAssets.filter(asset => asset.tradable == true)

        let symbols = []
        for (let stock of activeAssets) {
            symbols.push(stock.symbol)
        }

        return symbols

    }

    /*
    * Input: array<symbol>
    * 
    * Description: Removes symbols from websocket Quotes list
    * 
    * FIXME: This needs verification for the symbol
    */
    async getStockQuote(symbol) {

        let requestOptions = {
            method: 'GET',
            headers: {
                "Apca-Api-Key-Id": this.#api_config.key_id,
                "Apca-Api-Secret-Key": this.#api_config.secret
            },
        };
        let response = await fetch("https://data.alpaca.markets/v2/stocks/" + symbol + "/quotes/latest", requestOptions)
        let result = await response.json()
        //check error 
        if (result.hasOwnProperty("code")) {
            return null
        }

        let quote = result.quote.ap;

        return quote

    }

    /*
    * Input: symbol
    * 
    * Description: checks to see if symbol is avlible on alpaca api
    * 
    */
    async verifySymbol(symbol) {
        const symbols = await this.getAllStockSymbols()
        if (symbols.includes(symbol)) {
            return true;
        }
        return false;
    }



    //Getters below

    getSubscribedQuotes() {
        return Object.keys(this.#subscribedQuotes);
    }

    getMarketData() {
        return this.marketData
    }




}
module.exports = Alpaca;



/*
//testing

async function testing(){
    const newAplaca = new Alpaca(true)
    //await newAplaca.subscribeQuotes(['AAPL'])
    //await newAplaca.unSubscribeQuotes(['GOOGL'])
    await newAplaca.subscribeQuotes(['TSLA'], 100)
    //await newAplaca.unSubscribeQuotes(['AAPL'])

    await newAplaca.getStockQuote('AAPL').then(function (data){
        console.log(data)
    })

    await newAplaca.getAllStockSymbols('AAPL').then(function (data){
        console.log(data)
    })

    let result = await newAplaca.verifySymbol('AAPL')
    console.log(result)

    result = await newAplaca.verifySymbol('DOESNOTEXIST')
    console.log(result)

    let interval_runs = 0;
    async function reset(){
        await newAplaca.resetSubscriptionQuoteTimeout('TSLA', 5000);
        console.log('Run Reset')
        
        if (interval_runs == 10){
            clearInterval(resetInterval)
        }
        interval_runs+=1
    }

    let resetInterval = setInterval(reset, 2000)
}

testing()
*/