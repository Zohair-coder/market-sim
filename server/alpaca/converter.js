

const fs = require('fs');

const stock_csv = require("./stocks.cjs");
const stock_json = require("./stocks.json");

//https://stackoverflow.com/questions/7431268/how-to-read-data-from-csv-file-using-javascript
function csvJSON(csv){

    let lines=csv.split("\n");
  
    let result = [];
  
    // NOTE: If your columns contain commas in their values, you'll need
    // to deal with those before doing the next step 
    // (you might convert them to &&& or something, then covert them back later)
    // jsfiddle showing the issue https://jsfiddle.net/
    let headers=lines[0].split(",");
  
    for(let i=1;i<lines.length;i++){
  
        let obj = {};
        let currentline=lines[i].split(",");
  
        for(let j=0;j<headers.length;j++){
            obj[headers[j]] = currentline[j];
        }
  
        result.push(obj);
  
    }
    //let json_result = {}
    //for (let stock of result){
    //    if (stock["ACT Symbol"].includes('$')){
    //        continue
    //    }
    //    json_result[stock["ACT Symbol"]] = stock
    //}
  
    //return result; //JavaScript object
    return JSON.stringify(result, null, 4); //JSON
  }

let data = csvJSON(stock_csv)

fs.writeFile('stocks.json', data, (err) => {
    if (err) {
        throw err;
    }
    console.log("JSON data is saved.");
});
console.log(Object.keys(stock_json))
