
const Alpaca = require("./Alpaca")

if (!global.alpacaSocket) {
    global.alpacaSocket = new Alpaca(true);
    module.exports = alpacaSocket;
} else {
    module.exports = global.alpacaSocket;
}

