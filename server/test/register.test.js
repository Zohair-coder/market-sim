const Register = require("../api/auth/register");
const Testing = require("./testing")
const testing = new Testing()

describe("Register", () => {

    const register = new Register();
    beforeAll(async () => { await testing.initDatabase(); });
    afterAll(async () => { await register.db.pool.end() });

    describe("testing register", () => {
        test('defines register()', () => {
            expect(typeof register.register).toBe("function");
        });

        test('register() returns true if no errors', async () => {
            let username = "abc123";
            let password = "abc123";
            expect(await register.register(username, password)).toBe(null);
        });

    });

    describe("testing validateRegistration()", () => {

        test("define validateRegistration()", () => {
            expect(typeof register.validateRegistration).toBe("function");
        });

        test("validateRegistration returns empty array if no errors", async () => {
            let username = "abcd123";
            let password = "abc123456789";
            expect(await register.validateRegistration(username, password)).toEqual([]);
        })

        test("validateRegistration returns error if less than 8 character password", async () => {
            let username = "abcd123";
            let password = "1234567";
            expect(await register.validateRegistration(username, password)).toEqual(["Password must be at least 8 characters long"]);
        })

        test("validateRegistration returns error if no password", async () => {
            let username = "abcd123";
            let password = undefined;
            expect(await register.validateRegistration(username, password)).toEqual(["Password is required"]);
        })

    })
});
