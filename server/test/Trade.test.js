const Trade = require("../api/trade/Trade");
const trade = new Trade();
const Testing = require("./testing")
const testing = new Testing()

describe("Trade Testing", () => {

    beforeAll(async () => { await testing.initDatabase(); });
    afterAll(async () => { await trade.database.pool.end(); 
                            delete trade.alpaca});
    
    describe("testing validateUser", () => {
        test('defines valiodateUser()', () => {
            expect(typeof trade.validateUser).toBe("function");
        });
        
        test('existing user returns true', () => {
            return trade.validateUser("dora1").then(value => {
                expect(value).toEqual(true);
            })
        })

        test('non-existing user returns false', () => {
            return trade.validateUser("noExist").then(value => {
                expect(value).toEqual(false);
            })
        })

        test('user with too long of name returns false', () => {
            return trade.validateUser("abcdefghijklmnopqrstuvwxyz").then(value => {
                expect(value).toEqual(false);
            })
        })

    });

    describe("testing validateTypes", () => {
        test('defines validateTypes()', () => {
            expect(typeof trade.validateTypes).toBe("function");
        });
        
        test('all correct types returns true', () => {
            expect(trade.validateTypes("string", "string", 1.1, 1.1)).toEqual(true);
        })

        test('wrong user type returns false', () => {
            expect(trade.validateTypes(1, "string", 1.1, 1.1)).toEqual(false);
        })
        test('wrong stock type returns false', () => {
            expect(trade.validateTypes("string", 1, 1.1, 1.1)).toEqual(false);
        })
        test('wrong amount type returns false', () => {
            expect( trade.validateTypes("string", "string", "wrong", 1.1))
        })
        test('wrong price type returns false', () => {
            expect(trade.validateTypes("string", "string", 1.1, "wrong")).toEqual(false);
        })
        test('multiple wrong type returns false', () => {
                expect(trade.validateTypes(23.2, "string", "wrong", 1.1)).toEqual(false);
        })

    });

    describe("testing validateStock", () => {
        test('defines validateStock()', () => {
            expect(typeof trade.validateStock).toBe("function");
        });
        
        test('valid stock symbol returns true', () => {
            return trade.validateStock("AAPL").then(value => {
                expect(value).toEqual(true);
            })
        })

        test('too long stock symbol returns false', () => {
            return trade.validateStock("DOESNOTEXIST").then(value => {
                expect(value).toEqual(false);
            })
        })

        test('non-existant stock symbol returns false', () => {
            return trade.validateStock("XXXZZZYYY").then(value => {
                expect(value).toEqual(false);
            })
        })

    });

    describe("testing validatePrice", () => {

        let aaplVal;
        beforeAll(async () => { aaplVal = await trade.alpaca.getStockQuote('AAPL') });

        test('defines validatePrice()', () => {
            expect(typeof trade.validatePrice).toBe("function");
        });
        
        test('valid price returns true', () => {
            return trade.validatePrice("AAPL" , aaplVal).then(value => {
                expect(value).toEqual(true);
            })
        })

        test('too low of stock price is invalid', () => {
            return trade.validatePrice("AAPL", 0.23).then(value => {
                expect(value).toEqual(false);
            })
        })

        test('too high of stock price is invalid', () => {
            return trade.validatePrice("AAPL", 9999999).then(value => {
                expect(value).toEqual(false);
            })
        })

    });

});