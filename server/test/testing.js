let pg = require('pg');
let fs = require('fs');
const db_config = require("../config/db_config");

const SQL_FILES = ['./database/populate_sample1.sql']


class Testing{

    constructor (){
    }

    async initDatabase(){
        
        this.pool = new pg.Pool(db_config)

        await this.initSQLFile(this.pool, './database/create_tables.sql');

        for (let file of SQL_FILES){
            await this.initSQLFile(this.pool, file)
        }

        await this.pool.end()
        return
    }

    async initSQLFile(pool, file) {
        let sql = fs.readFileSync(file).toString();
        await pool.query(await this.stripSQLString(sql))
        return
    }

    async stripSQLString(sql) {
        return sql.replace(/(\r\n|\n|\r|\t)/gm, " ").replace(/\s\s+/g, " ");
    }
}
module.exports = Testing