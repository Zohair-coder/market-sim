const Database = require('../database/Database');
const db_config = require("../config/db_config");


const Portfolio = require("../portfolio/portfolio");

describe("Portfolio", () => {
    const database = new Database(db_config);
    beforeAll( async () => {await initDatabase();});
    afterAll( async () => {await database.pool.end()});

    const portfolio = new Portfolio(database);

    describe("getPortfolioData test", () => {
        beforeEach( async () => {await initDatabase();});

        test('defines getPortfolioData()', () => {
            expect(typeof database.getPortfolioData).toBe("function");
        });

        test('Existing username returns existing portfolio', () => {
            return database.getUserStocks("dora1").then(value => {
                expect(value['username']).toEqual("dora1");
                expect(value['balance']).toEqual("10000");
                expect(value['stocks']).toEqual({
                    "stocks" : 
                    [
                        {
                        "sid": 1,
                        "uid": 1,
                        "symbol": "AAPL",
                        "amount": "2.1",
                        "current_price": 176.6,
                        "returns": 12.240000000000066
                        }
                    ]
                });
                expect(value['trades']).toBeDefined();
                expect(value["watch"]).toEqual({
                    "watch": [
                        {
                          "wid": 1,
                          "uid": 1,
                          "symbol": "AMD",
                          "current_price": 176.6
                        }
                      ]
                });
            });
        });

    });
});
