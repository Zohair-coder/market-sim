const Database = require('../database/Database');
const db_config = require("../config/db_config");
const Testing = require("./testing")
const testing = new Testing()




/* Tests set to work with sample1 data */
describe("Database", () => {

    const database = new Database(db_config);
    beforeAll(async () => { await testing.initDatabase(); });
    afterAll(async () => { await database.pool.end() });

    describe("getUserStocks test", () => {
        
        beforeEach(async () => { await testing.initDatabase(); });

        test('defines getUserStocks()', () => {
            expect(typeof database.getUserStocks).toBe("function");
        });

        test('Existing username returns existing stocks', () => {
            return database.getUserStocks("dora1").then(value => {
                expect(value.length).toEqual(1);
                expect(value[0]).toEqual({
                    "amount": "2.1",
                    "sid": 1,
                    "symbol": "AAPL",
                    "uid": 1
                });
            })
        })

        test('Existing uid returns existing stocks', () => {
            return database.getUserStocks(1).then(value => {
                expect(value.length).toEqual(1);
                expect(value[0]).toEqual({
                    "amount": "2.1",
                    "sid": 1,
                    "symbol": "AAPL",
                    "uid": 1
                });
            })
        })

        test('non-existant username returns empty', () => {
            return database.getUserStocks("non-existant").then(value => {
                expect(value).toEqual([]);
            })
        })

        test('empty username returns empty', () => {
            return database.getUserStocks("").then(value => {
                expect(value).toEqual([]);
            })
        })
    });


    describe("getUserBalance test", () => {

        beforeEach(async () => { await testing.initDatabase(); });

        test('defines getUserBalance()', () => {
            expect(typeof database.getUserBalance).toBe("function");
        });

        test('Existing username returns existing balance', () => {
            return database.getUserBalance("dora1").then(value => {
                expect(value).toEqual(10000);
            })
        })

        test('non-existant uid returns empty', () => {
            return database.getUserBalance(25).then(value => {
                expect(value).toEqual(null);
            })
        })

        test('Existing username returns existing balance', () => {
            return database.getUserBalance(1).then(value => {
                expect(value).toEqual(10000);
            })
        })

        test('non-existant username returns empty', () => {
            return database.getUserBalance("non-existant").then(value => {
                expect(value).toEqual(null);
            })
        })

        test('empty user returns empty', () => {
            return database.getUserBalance().then(value => {
                expect(value).toEqual(null);
            })
        })
    });

    describe("setUserBalance tests", () => {

        test('defines setUserBalance()', () => {
            expect(typeof database.setUserBalance).toBe("function");
        });

        test('user balance successfully set with uid', () => {
            return database.setUserBalance(1, 7500).then(value => {
                expect(value).toEqual(true);
            })
        })

        test('Check balance is successfully set with uid', () => {
            return database.getUserBalance(1).then(value => {
                expect(value).toEqual(7500);
            })
        })

        test('user balance successfully set with username', () => {
            return database.setUserBalance("dora1", 5000).then(value => {
                expect(value).toEqual(true);
            })
        })

        test('Check balance is successfully set with username', () => {
            return database.getUserBalance("dora1").then(value => {
                expect(value).toEqual(5000);
            })
        })

    });

    describe("getSID test", () => {

        beforeEach(async () => { await testing.initDatabase(); });

        test('defines getSID()', () => {
            expect(typeof database.getSID).toBe("function");
        });

        test('Existing uid and symbol returns existing ', () => {
            return database.getSID(1, 'AAPL').then(value => {
                expect(value).toEqual(1);
            })
        })

        test('non-Existing uid returns null ', () => {
            return database.getSID(99, 'AAPL').then(value => {
                expect(value).toEqual(null);
            })
        })

        test(' non-Existing symbol returns null ', () => {
            return database.getSID(1, 'AABBCCDD').then(value => {
                expect(value).toEqual(null);
            })
        })
    });

    describe("getUID test", () => {

        beforeEach(async () => { await testing.initDatabase(); });

        test('defines getUID()', () => {
            expect(typeof database.getUID).toBe("function");
        });

        test('Existing username returns existing uid', () => {
            return database.getUID("dora1").then(value => {
                expect(value).toEqual(1);
            })
        })

        test('non-Existing username returns null ', () => {
            return database.getUID("banana").then(value => {
                expect(value).toEqual(null);
            })
        })

        test(' no username returns null ', () => {
            return database.getUID().then(value => {
                expect(value).toEqual(null);
            })
        })
    });

    describe("getStockAmount test", () => {

        beforeEach(async () => { await testing.initDatabase(); });

        test('defines getStockAmount()', () => {
            expect(typeof database.getStockAmount).toBe("function");
        });

        test('Existing sid returns existing stock amount', () => {
            return database.getStockAmount(1).then(value => {
                expect(value).toEqual(2.1);
            })
        })

        test('non-Existing sid returns null ', () => {
            return database.getStockAmount(99).then(value => {
                expect(value).toEqual(null);
            })
        })

        test('no stock amount returns null', () => {
            return database.getStockAmount().then(value => {
                expect(value).toEqual(null);
            })
        })
    });

    describe("setStockAmount tests", () => {

        test('defines setStockAmount()', () => {
            expect(typeof database.setStockAmount).toBe("function");
        });

        test('stock amount successfully set with uid', () => {
            return database.setStockAmount(1, 3.5).then(value => {
                expect(value).toEqual(true);
            })
        })

        test('Check stock amount is successfully set with uid', () => {
            return database.getStockAmount(1).then(value => {
                expect(value).toEqual(3.5);
            })
        })


    });

    describe("getUserTrades tests", () => {

        beforeEach(async () => { await testing.initDatabase(); });

        test('defines getUserTrades()', () => {
            expect(typeof database.getUserTrades).toBe("function");
        });

        test('Existing uid returns trade list ', () => {
            return database.getUserTrades(1).then(value => {
                for (let obj of value) {
                    delete obj.time
                }
                expect(value.length).toEqual(2);
                expect(value[0]).toEqual({ "amount": "3.1", "price": "172.2", "symbol": "AAPL", "tid": 1, "type": "buy", "uid": 1 });
                expect(value[1]).toEqual({ "amount": "1.0", "price": "175.2", "symbol": "AAPL", "tid": 3, "type": "sell", "uid": 1 });
            })
        })

        test('Non-Existing uid returns null', () => {
            return database.getUserTrades(20).then(value => {
                expect(value).toEqual([]);
            })
        })

        test('empty uid returns null', () => {
            return database.getUserTrades().then(value => {
                expect(value).toEqual([]);
            })
        })


    });

    describe("addTrade tests", () => {

        test('defines addTrade()', () => {
            expect(typeof database.addTrade).toBe("function");
        });

        test('trade successfully added to database', () => {
            return database.addTrade(1, 'buy', 'AAPL', 172.39, 2.32).then(value => {
                expect(value).toEqual(true);
            })
        })

        test('Check trade is successfully added to database', () => {
            return database.getUserTrades(1).then(value => {
                for (let obj of value) {
                    delete obj.time
                }
                expect(value.length).toEqual(3);
                expect(value[0]).toEqual({ "amount": "3.1", "price": "172.2", "symbol": "AAPL", "tid": 1, "type": "buy", "uid": 1 });
                expect(value[1]).toEqual({ "amount": "1.0", "price": "175.2", "symbol": "AAPL", "tid": 3, "type": "sell", "uid": 1 });
                expect(value[2]).toEqual({ "amount": "2.32", "price": "172.39", "symbol": "AAPL", "tid": 8, "type": "buy", "uid": 1 });
            })
        })

    });

    
    describe("addStock tests", () => {

        test('defines addStock()', () => {
            expect(typeof database.addStock).toBe("function");
        });

        test('trade successfully added to database', () => {
            return database.addStock(1,'GOOGL', 2.32).then(value => {
                expect(value).toEqual(true);
            })
        })

        test('Check stock is successfully added to database', () => {
            return database.getUserStocks(1).then(value => {
                for (let obj of value) {
                    delete obj.time
                }
                expect(value.length).toEqual(2);
                expect(value[0]).toEqual({ "amount": "2.1", "symbol": "AAPL", "sid": 1, "uid": 1 });
                expect(value[1]).toEqual({ "amount": "2.32", "symbol": "GOOGL", "sid": 6, "uid": 1 });
            })
        })

    });

});


