const Sell = require("../api/trade/Sell");
const sell = new Sell();
const Testing = require("./testing")
const testing = new Testing()

describe("Sell Testing", () => {

    beforeAll(async () => { await testing.initDatabase(); });
    afterAll(async () => { await sell.database.pool.end() 
                            delete sell.alpaca});

    describe("testing validateSellPower", () => {
        test('defines validateSellPower()', () => {
            expect(typeof sell.validateSellPower).toBe("function");
        });

        describe("testing validateSellPower", () => {
            test('defines validateSellPower()', () => {
                expect(typeof sell.validateSellPower).toBe("function");
            });
            
            test('user having enough sell power returns true', () => {
                return sell.validateSellPower("dora1", "AAPL", 0.32).then(value => {
                    expect(value).toEqual(true);
                })
            })
    
            test('user not having enough sell power returns false', () => {
                return sell.validateSellPower("dora1", "AAPL", 23.32).then(value => {
                    expect(value).toEqual(false);
                })
            })
        });

        describe("testing sellValidator", () => {

            let aaplVal;
            beforeAll(async () => { aaplVal = await sell.alpaca.getStockQuote('AAPL') });

            test('defines sellValidator()', () => {
                expect(typeof sell.sellValidator).toBe("function");
            });

            test('valid sell returns with no errors', () => {
                return sell.sellValidator("dora1",  'AAPL', 1.32, aaplVal).then(value => {
                    expect(value).toEqual(null);
                })
            })

            test('invalid stock returns error', () => {
                return sell.sellValidator("dora1",  'AAPLFSRE', 4.32, aaplVal).then(value => {
                    expect(value).toEqual("This Stock is Not Avalible to Purchase");
                })
            })

            test('invalid input returns error', () => {
                return sell.sellValidator("dora1",  32, '324.32', aaplVal).then(value => {
                    expect(value).toEqual("Invalid Input");
                })
            })

            test('invalid username returns error', () => {
                return sell.sellValidator("banana",  'AAPL', 4.32, aaplVal).then(value => {
                    expect(value).toEqual("Invalid Username");
                })
            })
            
            test('Insufficient Stocks returns error', () => {
                return sell.sellValidator("dora1",  'AAPL', 324.32, aaplVal).then(value => {
                    expect(value).toEqual("Insufficient Stocks");
                })
            })

            
            test('too large amount returns error', () => {
                return sell.sellValidator("dora1",  'AAPL', 999999999999.32, aaplVal).then(value => {
                    expect(value).toEqual("Invalid Amount");
                })
            })

            test('too small amount returns error', () => {
                return sell.sellValidator("dora1",  'AAPL', 0.0000000002, aaplVal).then(value => {
                    expect(value).toEqual("Invalid Amount");
                })
            })

            test('too high of price is invalid', () => {
                return sell.sellValidator("dora1",  'AAPL', .1, 99999999).then(value => {
                    expect(value).toEqual("Invalid Price");
                })
            })

            test('too low of price is invalid', () => {
                return sell.sellValidator("dora1",  'AAPL', .1, 0.001).then(value => {
                    expect(value).toEqual("Invalid Price");
                })
            })
        });

        describe("testing validateAmount", () => {
            test('defines validateAmount()', () => {
                expect(typeof sell.validateAmount).toBe("function");
            });

            test('valid amount returns true', () => {
                expect(sell.validateAmount(14)).toEqual(true);
            })
            
            test('too large amount returns false', () => {
                expect(sell.validateAmount(9999999999999)).toEqual(false);
            })

            test('too small amount returns false', () => {
                expect(sell.validateAmount(0.0000000002)).toEqual(false);
            })
        });

        describe("testing sell", () => {

            let aaplVal;
            beforeAll(async () => { 
                await testing.initDatabase(); 
                aaplVal = await sell.alpaca.getStockQuote('AAPL')
            });

            test('defines sell()', () => {
                expect(typeof sell.sell).toBe("function");
            });

            test('valid sell returns with no errors', () => {
                return sell.sell("dora1",  'AAPL', 0.42, aaplVal).then(value => {
                    expect(value).toEqual({"status": "OK","error": null});
                })
            })

            test('Check stock is successfully added to database', () => {
                return sell.database.getUserStocks(1).then(value => {
                    for (let obj of value) {
                        delete obj.time
                    }
                    expect(value.length).toEqual(1);
                    value[0].amount = parseFloat(value[0].amount).toFixed(3);
                    expect(value[0]).toEqual({ "amount": "1.680", "symbol": "AAPL", "sid": 1, "uid": 1 });
                })
            })

            test('Check trade is successfully added to database', () => {
                return sell.database.getUserTrades(1).then(value => {
                    for (let obj of value) {
                        delete obj.time
                    }
                    expect(value.length).toEqual(3);
                    expect(value[0]).toEqual({ "amount": "3.1", "price": "172.2", "symbol": "AAPL", "tid": 1, "type": "buy", "uid": 1 });
                    expect(value[1]).toEqual({ "amount": "1.0", "price": "175.2", "symbol": "AAPL", "tid": 3, "type": "sell", "uid": 1 });
                    expect(value[2]).toEqual({ "amount": "0.42", "price": aaplVal.toString(), "symbol": "AAPL", "tid": 8, "type": "sell", "uid": 1 });
                })
            })


            test('invalid input returns error', () => {
                return sell.sell("dora1",  32, '324.32', aaplVal).then(value => {
                    expect(value).toEqual({"status":"NOT_OK", "error": "Invalid Input"});
                })
            })

            test('invalid username returns error', () => {
                return sell.sell("banana",  'AAPL', 4.32, aaplVal).then(value => {
                    expect(value).toEqual({"status":"NOT_OK", "error": "Invalid Username"});
                })
            })
            
            test('Insufficient Funds returns error', () => {
                return sell.sell("dora1",  'AAPL', 324.32, aaplVal).then(value => {
                    expect(value).toEqual({"status":"NOT_OK", "error": "Insufficient Stocks"});
                })
            })

            test('invalid price returns error', () => {
                return sell.sell("dora1",  'AAPL', 0.32, aaplVal*2).then(value => {
                    expect(value).toEqual({"status":"NOT_OK", "error": "Invalid Price"});
                })
            })
        });
    });
});
