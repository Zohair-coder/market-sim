const Buy = require("../api/trade/Buy");
const buy = new Buy();
const Testing = require("./testing")
const testing = new Testing()

describe("Buy Testing", () => {

    beforeAll(async () => { await testing.initDatabase(); });
    afterAll(async () => { await buy.database.pool.end() 
                            delete buy.alpacas});

    describe("testing validateBuyPower", () => {
        test('defines validateBuyPower()', () => {
            expect(typeof buy.validateBuyPower).toBe("function");
        });

        describe("testing validateBuyPower", () => {
            test('defines validateBuyPower()', () => {
                expect(typeof buy.validateBuyPower).toBe("function");
            });
            
            test('user having enough buy power returns true', () => {
                return buy.validateBuyPower("dora1", 1.32, 323.32).then(value => {
                    expect(value).toEqual(true);
                })
            })
    
            test('user not having enough buy power returns false', () => {
                return buy.validateBuyPower("dora1", 4.32, 3323.32).then(value => {
                    expect(value).toEqual(false);
                })
            })
        });

        describe("testing buyValidator", () => {

            let aaplVal;
            beforeAll(async () => { aaplVal = await buy.alpaca.getStockQuote('AAPL') });

            test('defines buyValidator()', () => {
                expect(typeof buy.buyValidator).toBe("function");
            });

            test('valid buy returns with no errors', () => {
                return buy.buyValidator("dora1",  'AAPL', 4.32, aaplVal).then(value => {
                    expect(value).toEqual(null);
                })
            })

            test('invalid stock returns error', () => {
                return buy.buyValidator("dora1",  'AAPLFSRE', 4.32, aaplVal).then(value => {
                    expect(value).toEqual("This Stock is Not Avalible to Purchase");
                })
            })

            test('invalid input returns error', () => {
                return buy.buyValidator("dora1",  32, '324.32', aaplVal).then(value => {
                    expect(value).toEqual("Invalid Input");
                })
            })

            test('invalid username returns error', () => {
                return buy.buyValidator("banana",  'AAPL', 4.32, aaplVal).then(value => {
                    expect(value).toEqual("Invalid Username");
                })
            })
            
            test('Insufficient Funds returns error', () => {
                return buy.buyValidator("dora1",  'AAPL', 324.32, aaplVal).then(value => {
                    expect(value).toEqual("Insufficient Funds");
                })
            })

            test('too large amount returns error', () => {
                return buy.buyValidator("dora1",  'AAPL', 999999999.32, aaplVal).then(value => {
                    expect(value).toEqual("Invalid Amount");
                })
            })

            test('too small amount returns error', () => {
                return buy.buyValidator("dora1",  'AAPL', 0.0000000002, aaplVal).then(value => {
                    expect(value).toEqual("Invalid Amount");
                })
            })

            
            test('too high of price is invalid', () => {
                return buy.buyValidator("dora1",  'AAPL', 3.1, 99999999).then(value => {
                    expect(value).toEqual("Invalid Price");
                })
            })

            test('too low of price is invalid', () => {
                return buy.buyValidator("dora1",  'AAPL', 3.1, 0.001).then(value => {
                    expect(value).toEqual("Invalid Price");
                })
            })

        });

        describe("testing validateAmount", () => {
            test('defines validateAmount()', () => {
                expect(typeof buy.validateAmount).toBe("function");
            });

            test('valid amount returns true', () => {
                expect(buy.validateAmount(14)).toEqual(true);
            })
            
            test('too large amount returns false', () => {
                expect(buy.validateAmount(9999999999999)).toEqual(false);
            })

            test('too small amount returns false', () => {
                expect(buy.validateAmount(0.0000000002)).toEqual(false);
            })
        });

        describe("testing buy", () => {

            let aaplVal;
            let googlVal;
            beforeAll(async () => { 
                await testing.initDatabase(); 
                aaplVal = await buy.alpaca.getStockQuote('AAPL')
                googlVal = await buy.alpaca.getStockQuote('GOOGL')
            });

            test('defines buy()', () => {
                expect(typeof buy.buy).toBe("function");
            });

            test('valid buy returns with no errors of not held stock', () => {
                return buy.buy("dora1",  'GOOGL', 2.32, googlVal).then(value => {
                    expect(value).toEqual({"status": "OK","error": null});
                })
            })

            test('Check stock is successfully added to database', () => {
                return buy.database.getUserStocks(1).then(value => {
                    for (let obj of value) {
                        delete obj.time
                    }
                    expect(value.length).toEqual(2);
                    expect(value[0]).toEqual({ "amount": "2.1", "symbol": "AAPL", "sid": 1, "uid": 1 });
                    expect(value[1]).toEqual({ "amount": "2.32", "symbol": "GOOGL", "sid": 6, "uid": 1 });
                })
            })

            test('Check trade is successfully added to database', () => {
                return buy.database.getUserTrades(1).then(value => {
                    for (let obj of value) {
                        delete obj.time
                    }
                    expect(value.length).toEqual(3);
                    expect(value[0]).toEqual({ "amount": "3.1", "price": "172.2", "symbol": "AAPL", "tid": 1, "type": "buy", "uid": 1 });
                    expect(value[1]).toEqual({ "amount": "1.0", "price": "175.2", "symbol": "AAPL", "tid": 3, "type": "sell", "uid": 1 });
                    expect(value[2]).toEqual({ "amount": "2.32", "price": googlVal.toString(), "symbol": "GOOGL", "tid": 8, "type": "buy", "uid": 1 });
                })
            })

            test('valid buy returns with no errors of held stock', () => {
                return buy.buy("dora1",  'AAPL', 3.22, aaplVal).then(value => {
                    expect(value).toEqual({"status": "OK","error": null});
                })
            })

            test('Check stock is successfully added to database', () => {
                return buy.database.getUserStocks(1).then(value => {
                    for (let obj of value) {
                        delete obj.time
                    }
                    expect(value.length).toEqual(2);
                    expect(value[0]).toEqual({ "amount": "5.32", "symbol": "AAPL", "sid": 1, "uid": 1 });
                    expect(value[1]).toEqual({ "amount": "2.32", "symbol": "GOOGL", "sid": 6, "uid": 1 });
                })
            })

            test('Check trade is successfully added to database', () => {
                return buy.database.getUserTrades(1).then(value => {
                    for (let obj of value) {
                        delete obj.time
                    }
                    expect(value.length).toEqual(4);
                    expect(value[0]).toEqual({ "amount": "3.1", "price": "172.2", "symbol": "AAPL", "tid": 1, "type": "buy", "uid": 1 });
                    expect(value[1]).toEqual({ "amount": "1.0", "price": "175.2", "symbol": "AAPL", "tid": 3, "type": "sell", "uid": 1 });
                    expect(value[2]).toEqual({ "amount": "2.32", "price": googlVal.toString(), "symbol": "GOOGL", "tid": 8, "type": "buy", "uid": 1 });
                    expect(value[3]).toEqual({ "amount": "3.22", "price": aaplVal.toString(), "symbol": "AAPL", "tid": 9, "type": "buy", "uid": 1 });
                })
            })

            test('invalid stock returns error', () => {
                return buy.buy("dora1",  'AAPLFSRE', 4.32, aaplVal).then(value => {
                    expect(value).toEqual({"status":"NOT_OK", "error": "This Stock is Not Avalible to Purchase"});
                })
            })

            test('invalid input returns error', () => {
                return buy.buy("dora1",  32, '324.32', aaplVal).then(value => {
                    expect(value).toEqual({"status":"NOT_OK", "error": "Invalid Input"});
                })
            })

            test('invalid username returns error', () => {
                return buy.buy("banana",  'AAPL', 4.32, aaplVal).then(value => {
                    expect(value).toEqual({"status":"NOT_OK", "error": "Invalid Username"});
                })
            })
            
            test('Insufficient Funds returns error', () => {
                return buy.buy("dora1",  'AAPL', 324.32, aaplVal).then(value => {
                    expect(value).toEqual({"status":"NOT_OK", "error": "Insufficient Funds"});
                })
            })

            test('invalid price returns error', () => {
                return buy.buy("dora1",  'AAPL', 0.32, aaplVal*2).then(value => {
                    expect(value).toEqual({"status":"NOT_OK", "error": "Invalid Price"});
                })
            })
        });
    });
});
