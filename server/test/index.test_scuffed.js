const request = require("supertest");

const { app } = require("../index");
const { server } = require("../index")


//https://dev.to/franciscomendes10866/testing-express-api-with-jest-and-supertest-3gf 
describe("Test index.html", () => {
  afterAll(async () => { await server.close() });

  describe("GET /auth tests", () => {
    test("regular ", (done) => {
      request(app)
        .get("/auth")
        .expect("Content-Type", 'application/json; charset=utf-8')
        .expect('true')
        .expect((res) => {
          res.body = {};
        })
        .end((err, res) => {
          if (err) return done(err);
          return done();
        });
    });
  });
/*
  describe("GET /portfolio tests", () => {

    test("regular response type", (done) => {
      request(app)
        .get("/portfolio")
        .expect("Content-Type", 'text/plain; charset=utf-8')
        .end((err, res) => {
          if (err) return done(err);
          return done();
        });
    });

    test("empty username response", (done) => {
      request(app)
        .get("/portfolio")
        .send({
          user: "",
        })
        .expect('Bad Request')
        .end((err, res) => {
          if (err) return done(err);
          return done();
        });
    });

    test("bad username response", (done) => {
      request(app)
        .get("/portfolio")
        .send({
          user: "",
        })
        .expect('Bad Request')
        .end((err, res) => {
          if (err) return done(err);
          return done();
        });
    });

    test("good username response", (done) => {
      request(app)
        .get("/portfolio")
        .send({
          user: "dora1",
        })
        .expect((res) => {
          res.body = { balance: 10000 };
        })
        .end((err, res) => {
          if (err) return done(err);
          return done();
        });
    });
  });
  */
});
