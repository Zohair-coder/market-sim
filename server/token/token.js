const axios = require("axios");

function verifyToken(token) {
    const config = {
        method: 'get',
        url: 'http://localhost:8080/api/auth/verify',
        headers: {
            'x-auth': token
        }
    };

    return axios(config);
}

module.exports = { verifyToken };