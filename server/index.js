const express = require('express');

const api_config = require("./config/api_config");
const db_config = require("./config/db_config");
const Database = require("./database/database");

const app = express();

const database = new Database(db_config)

app.use(express.json());

const PORT = process.env.PORT || 8080;

app.use("/api", require("./api/endpoints"));

let server = app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});


module.exports = { app, server };
