import React from "react";
import { BrowserRouter as Router, Route, Routes, Navigate } from "react-router-dom";
import Login from "./components/Login";
import Logout from "./components/Logout";
import Register from "./components/Register";
import Portfolio from "./components/Portfolio";
import Trade from "./components/Trade";
import About from "./components/About";
import Profile from "./components/Profile";
import Change from "./components/changePassword";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Navigate to="/login" />} />
        <Route path="login" element={<Login />}></Route>
        <Route path="logout" element={<Logout />}></Route>
        <Route path="register" element={<Register />}></Route>
        <Route path="portfolio" element={<Portfolio />}></Route>
        <Route path="trade" element={<Trade />}></Route>
        <Route path="about" element={<About />}></Route>
        <Route path="profile" element={<Profile />}></Route>
        <Route path="changePassword" element={<Change />}></Route>
        <Route
          path="*"
          element={
            <main style={{ padding: "1rem" }}>
              <p>There's nothing here!</p>
            </main>
          }
        />
      </Routes>
    </Router>
  );
}

export default App;
