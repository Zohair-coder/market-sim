import React from 'react'
import "./Portfolio.css"

function TradeRow(prop) {
    // copied from bmm hw 5
    function formatTimeStr(dateObj) {
        let M = "AM";
        let hour = dateObj.getHours();
        let minute = dateObj.getMinutes();
        
        if (hour > 12) {
            M = "PM";
            hour = dateObj.getHours() - 12;
        } else if (hour === 0) {
            hour = 12;
        } else if (hour === 12) {
            M = "PM";
        }
    
        if (minute.toString().length === 1) {
            minute = `0${minute.toString()}`;
        }
    
        return `${hour}:${minute} ${M}`;
    }

    function formatDateStr(dateStr) {
        const Days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        const Months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    
        let dateObj = new Date(dateStr);
        return `${Days[dateObj.getDay()]}, ${dateObj.getDate()} ${Months[dateObj.getMonth()]} ${dateObj.getFullYear()} @ ${formatTimeStr(dateObj)}`;
    }

    return (
        <tr>
            <td>{formatDateStr(prop.time)}</td>
            <td className={(prop.type === "buy") ? 'buy': 'sell'}>{prop.type}</td>
            <td>{prop.symbol}</td>
            <td>{prop.amount}</td>
            <td className='dollar'>${(prop.price) ? parseFloat(prop.price).toFixed(2) : "-"}</td>
        </tr>
    );
}

function TradeTable(prop) {
    let tablerows; 
    
    if (prop.isAuthenticated && prop.tradeList) {
        tablerows = prop.tradeList.map(trade => 
            <TradeRow 
                key={trade.tid}
                time={trade.time}
                type={trade.type}
                symbol={trade.symbol}
                amount={trade.amount}
                price={trade.price}
            />);
    }

    return (
        <table>
            <thead>
                <tr>
                    <th>Time</th>
                    <th>Type</th>
                    <th>Symbol</th>
                    <th>Amount</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody id="trade-table-body">
                {tablerows}
            </tbody>
        </table>
    )
}

export default TradeTable