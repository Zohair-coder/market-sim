import React, {useEffect, useState} from 'react'
import axios from "axios"
import Navbar from "./Navbar"
import { useNavigate } from 'react-router-dom'
import { checkPassword } from "../api/changePassword";
import { getUsername } from "../api/auth"

function ChangePassword() {

	const [input, setInput] = useState({});
	const [username, setUsername] = useState("");
    const [errors, setErrors] = useState([]);

    let navigate = useNavigate();

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setInput({ ...input, [name]: value });
    }

	const handleChangePassword = async (event) => {
		const token = localStorage.getItem("token");
		event.preventDefault();
		if(validateChange(input)) {
			await checkPassword(token, input.oldPass)
			.then(function (response) {
				if(response.data.valid) {
                    if(input.oldPass === input.newPass) {
                        errors.push("New password cannot be old password!")
                    }
                    else{               
                        updatePassword(input, username)
                        localStorage.removeItem("token")
                        navigate("/login")
                    }
				}
                setErrors(errors);
			})
			.catch(function(error) {
                let errors = [];
                if(input.oldPass !== input.newPass){ 
                    errors.push("Old password does not match!")
                    setErrors(errors);
            }
				console.log(error.response);
			}) 
		}
	}

	const validateChange = (userData) => {
		const newPass = userData.newPass;
		const conPass = userData.conPass;
		let errors = [];

		errors = errors.concat(validateNewPassword(newPass, conPass));

		setErrors(errors);
		return errors.length === 0;
	}


	const validateNewPassword = (password, confirmPassword) => {
        let errors = [];

        if (password !== confirmPassword) {
            errors.push("Passwords do not match!");
        }

        if (password === undefined) {
            errors.push("Password is required");
        }

        if (password.length < 8) {
            
            errors.push("Password must be at least 8 characters long!");
        }

        return errors;
    }

	const updatePassword = (userData, username) => {
		return axios.post("api/update", {
			username: username,
			password: userData.newPass
		})
	}

	useEffect(() => {
        const token = localStorage.getItem("token");
        if (token !== null) {
            getUsername(token)
                .then(function (response) {
                    if (response.data.status === "OK") {
                        setUsername(response.data.username);
                    }
                })
        }
    }, []);
	
	return (
		<>
            <Navbar />
            <div className="center exo-font">
                <h1>Change Password</h1>
                <form>
                    <div className="input-container">
                        <label>Old Password</label>
                        <input type="password" name="oldPass" onChange={handleInputChange} />
                    </div>
                    <div className="input-container">
                        <label>New Password</label>
                        <input type="password" name="newPass" onChange={handleInputChange} />
                    </div>
                    <div className="input-container">
                        <label>Confirm Password</label>
                        <input type="password" name="conPass" onChange={handleInputChange} />
                    </div>
                    <div className="errors">
                        {errors.map(error => {
                            return <p key={errors.indexOf(error)}>{error}</p>
                        })}
                    </div>
                    <button type="submit" onClick={handleChangePassword} >Change</button>
                </form>
            </div>
        </>
	)
}

export default ChangePassword
