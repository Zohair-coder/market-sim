import React, { useState, useEffect } from "react"
import Navbar from "./Navbar"
import "./Profile.css"
import { useNavigate } from 'react-router-dom'
import { getUserBalance } from "../api/profile"
import { deleteAccount } from "../api/delete"

function Profile() {
    let [balance, setBalance] = useState(0);
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    let navigate = useNavigate();

    const navChange = (() => {
        navigate("/changePassword");
    })

    const accountDelete = (() => {
        const isConfirmed = window.confirm("Are you sure you want to delete your account?");
        if (!isConfirmed) {
            return;
        }
        if (isAuthenticated) {
            deleteAccount(localStorage.getItem("token"))
                .then(function (response) {
                    localStorage.removeItem("token");
                    navigate("/login");
                })
                .catch(function (error) {
                    console.log(error.response);
                })
        }
    });

    useEffect(() => {
        if (localStorage.getItem("token")) {
            setIsAuthenticated(true);
            getUserBalance(localStorage.getItem("token"))
                .then(function (response) {
                    const balanceNumber = parseFloat(response.data.balance.balance);
                    if (isNaN(balanceNumber)) {
                        setBalance(0);
                    } else {
                        setBalance(balanceNumber.toFixed(2));
                    }
                })
                .catch(function (error) {
                    console.log(error.response);
                })
        }
    }, []);

    return (
        <div>
            <Navbar />
            <div className="center exo-font">
                <h2>Account Balance: ${balance}</h2>
                <div>
                    <button onClick={navChange}>Change Password</button>
                    <button onClick={accountDelete}>Delete Account</button>
                </div>
            </div>
        </div>
    )
}
export default Profile
