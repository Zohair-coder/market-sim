import React, { useState, useEffect } from 'react'
import Navbar from './Navbar'
import SearchBox from './SearchBox'
import TransactionForm from "./TransactionForm";
import { getSubscriptions, subscribe, resubscribe, getMarketData } from '../api/stocks'
import { getUserBalance } from "../api/profile"
import config from '../api_config';
import "./Trade.css"

function Trade() {
  const [balance, setBalance] = useState({ status: "loading", balance: 0 })
  const [marketData, setMarketData] = useState({});
  const [subscribedSymbols, setSubscribedSymbols] = useState([]);
  const [hasUserSearched, setHasUserSearched] = useState(false);
  const token = localStorage.getItem("token");

  const search = async (event, symbol) => {

    let subscriptions = (await getSubscriptions(token)).data.data;
    if (!subscriptions.includes(symbol)) {
      await subscribe(token, symbol, config["subscription_timeout"]);
    } else {
      await resubscribe(token, symbol, config["subscription_timeout"]);
    }
    setSubscribedSymbols([...subscribedSymbols, symbol]);
    setHasUserSearched(true);
  }

  useEffect(() => {
    if (token) {
      getUserBalance(token)
        .then(function (response) {
          const balanceNumber = parseFloat(response.data.balance.balance);
          if (isNaN(balanceNumber)) {
            setBalance({ status: "loading", balance: 0 });
          } else {
            setBalance({ status: "OK", balance: balanceNumber.toFixed(2) });
          }
        })
        .catch(function (error) {
          console.log(error.response);
        })
    }
  }, []);

  useEffect(() => {
    const intervalId = setInterval(() => {
      getMarketData(token)
        .then((response) => {
          setMarketData(response.data.data);
        })
    }, config["market_data_frequency"]);
    return () => clearInterval(intervalId);
  }, []);

  useEffect(() => {
    const intervalId = setInterval(() => {
      for (let symbol of subscribedSymbols) {
        resubscribe(token, symbol, config["subscription_timeout"]);
      }
    }, config["resubscribe_interval"]);

    return () => clearInterval(intervalId);
  }, [subscribedSymbols]);

  let stockData = null;
  if (subscribedSymbols.length > 0) {
    let symbol = getLastElement(subscribedSymbols);
    let price = parseFloat(marketData[symbol]);

    if (isNaN(price)) {
      price = 0;
    }
    price = price.toFixed(2);
    stockData = <>
      <div style={{ marginTop: "20px" }}>
        <h2 id="symbol">{symbol}</h2>
      </div>
      <h3 id="price">${price}</h3>
    </>
  }



  return (
    <>
      <Navbar />
      <div className='center exo-font' style={{
        width: "500px",
        marginTop: "100px"
      }}>
        <h1>Trade</h1>
        <h3>Balance: {(balance.status === "OK") ? `$${balance.balance}` : "loading..."}</h3>
        <SearchBox searchFunc={search} />
        {stockData}
        {hasUserSearched && <TransactionForm symbol={getLastElement(subscribedSymbols)} price={marketData[getLastElement(subscribedSymbols)]} />}
      </div>
    </>
  )
}

function getLastElement(array) {
  return array[array.length - 1];
}

export default Trade;
