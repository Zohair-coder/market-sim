import React from 'react'
import "./Portfolio.css"

function StockRow(stockProp) {
    if (stockProp.amount <= 0) {
        return (<></>);
    }

    const calculateTotal = () => {
        if (stockProp['current_price'] === undefined) {
            return "-";
        }
        return (stockProp.current_price * stockProp.amount).toFixed(2);
    }

    const calculateReturn = () => {
        if (stockProp['current_price'] === undefined) {
            return "-";
        }
        
        return (calculateTotal() - (stockProp.avg_purchase_price * stockProp.amount)).toFixed(2);
    }

    let current_price;
    if (stockProp['current_price'] === undefined) {
        current_price = "-";
    } else {
        current_price = stockProp['current_price'].toFixed(2);
    }

    function changeColor(e) {
        e.target.className = 'portfolioHover';
    }

    function revertColor(e) {
        e.target.className = 'watchBtn';
    }

    let returnTotal = calculateReturn();

    return (
      <tr>
        <td 
            stocksymbol={stockProp.symbol}
            onClick={stockProp.showTradeTableOnClick}
            onMouseOver={changeColor}
            onMouseLeave={revertColor}>
            {stockProp.symbol}
        </td>
        <td>{stockProp.symbol}</td>
        <td>{stockProp.amount}</td>
        <td className="dollar" style={((stockProp.prev_price === stockProp.current_price) || (stockProp.prev_price === undefined)) ? 
            {color: 'black'} : (stockProp.prev_price < stockProp.current_price) ? 
            {color: "green"} : {color: "red"}}>${current_price}</td>
        <td className="dollar">${calculateTotal()}</td>
        <td className="dollar">${stockProp.avg_purchase_price.toFixed(2)}</td>
        <td className={((returnTotal === 0) || (returnTotal === "-")) ? 'dollar' : ((returnTotal < 0) ? 'loss dollar': 'profit dollar')} >${returnTotal}</td>
      </tr>
    );
}

function PortfolioTable(prop) {
    let tablerows = null;

    if (prop.isAuthenticated && prop.stocksList && (Object.keys(prop.marketData).length > 0)) {
        tablerows = prop.stocksList.map(stock => 
            <StockRow 
                key={stock.sid}
                symbol={stock.symbol}
                amount={stock.amount}
                current_price={prop.marketData.cur[stock.symbol]}
                prev_price={prop.marketData.prev[stock.symbol]}
                avg_purchase_price={stock.avg_purchase_price}
                showTradeTableOnClick={prop.showTradeTableOnClick}
            />
        );
    }

    return (
        <table>
            <thead>
                <tr>
                    <th>Symbol</th>
                    <th>Name</th>
                    <th>Shares</th>
                    <th>Price</th>
                    <th>Total</th>
                    <th>Avg Cost</th>
                    <th>Return</th>
                </tr>
            </thead>
            <tbody id="portfolio-table-body">
                {tablerows}
            </tbody>
        </table>
    );
}

export default PortfolioTable