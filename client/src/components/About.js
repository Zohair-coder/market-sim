import React from "react"
import Navbar from "./Navbar.js"
import bernard from "./devImages/bernard.jpg"
import zohair from "./devImages/zohair.jpg"
import caleb from "./devImages/caleb.jpg"
import freddy from "./devImages/freddy.jpg"
import "./About.css" 

class About extends React.Component {
	constructor() {
		super()
		this.state = {}
	}

	render() {
		return (
			<div className="exo-font">
				<Navbar /><br/>
				<p className="info">Market Sim is a stock market simulator website that lets users trade stocks with fake currency. Our target audience is anyone interested in the stock market but don’t necessarily have the extra funds. Or people who want to experiment with the stock market but don’t want to gamble their money. Using real time market data from an API, users will be able to interact with the simulated stock market. They will be able to see their portfolio on the homepage to check whether their shares are in profit or not. Their goal is to maximize their capital. Users would be able to search for tickers and also trade them by buying or selling at market price. The database will keep track of the users accounts, trade history and their current portfolio.</p><br/><br/>
				<h2 className="devs">StackOverFlow^C^V Developers</h2>
				<div className="row">
					<div className="column">
						<img style={{boxShadow: "5px 5px 5px #000000"}} src={zohair} alt="Zohair" width="350px" height="400px"/>
						<p style={{fontWeight: "bold", fontSize: 22}}>Zohair Hasan</p>
						<p style={{fontStyle: "italic", fontSize: 22}}>Bachelor of Science in Computer Science</p>
					</div>
					<div className="column">
						<img style={{boxShadow: "5px 5px 5px #000000"}} src={bernard} alt="Bernard" width="350px" height="400px"/>
						<p style={{fontWeight: "bold", fontSize: 22}}>Bernard Morelos</p>
						<p style={{fontStyle: "italic", fontSize: 22}}>Bachelor of Science in Computer Engineering</p>
					</div>
					<div className="column">
						<img style={{boxShadow: "5px 5px 5px #000000"}} src={caleb} alt="Caleb" width="300px" height="400px"/>
						<p style={{fontWeight: "bold", fontSize: 22}}>Caleb Secor</p>
						<p style={{fontStyle: "italic", fontSize: 22}}>Bachelors of Science, Computer Engineering 
Master of Science, Cyber Security - CS track</p>
					</div>
					<div className="column">
						<img style={{boxShadow: "5px 5px 5px #000000"}} src={freddy} alt="Freddy" width="400px" height="400px"/>
						<p style={{fontWeight: "bold", fontSize: 22}}>Freddy Dudak</p>
						<p style={{fontStyle: "italic", fontSize: 22}}>Bachelor of Science in Computer Engineering</p>
					</div>
				</div>
			</div>
		)
	}
}
export default About
