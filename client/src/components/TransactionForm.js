import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Switch from '@mui/material/Switch';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import LoadingButton from '@mui/lab/LoadingButton';
import { buy, sell } from "../api/trade";
import { addWatch, removeWatch, getWatch } from "../api/watch";
import "./TransactionForm.css"


function TransactionForm(props) {

    const [input, setInput] = useState({
        "shares": 0,
        "price": 0,
    });
    const [isWatched, setIsWatched] = useState(false);
    const [error, setError] = useState(null);
    const [isBuying, setIsBuying] = useState(false);
    const [isSelling, setIsSelling] = useState(false);

    const token = localStorage.getItem("token");
    const navigate = useNavigate();

    useEffect(() => {
        updateIsWatched();
    }, [props.symbol]);

    const updateIsWatched = async () => {
        setIsWatched(await isAddedToWatchlist());
    }

    const handleInputChange = async (event) => {
        const { name, value } = event.target;
        console.log(input);

        if (name === "shares") {
            updatePrice(value);
        } else if (name === "price") {
            updateShares(value);
        }
    }

    const updatePrice = (shares) => {
        let shareNumber = parseFloat(shares);
        if (isNaN(shareNumber)) {
            setInput({ "shares": "", "price": 0 });
            return;
        }
        let pricePerShare = parseFloat(props.price);
        const newPrice = (shareNumber * pricePerShare).toFixed(2);
        setInput({ "shares": shareNumber, "price": newPrice });
    }

    const updateShares = (price) => {
        let priceNumber = parseFloat(price);
        if (isNaN(priceNumber)) {
            setInput({ "shares": 0, "price": "" });
            return;
        }
        let pricePerShare = parseFloat(props.price);
        const newShares = (priceNumber / pricePerShare).toFixed(2);
        setInput({ "shares": newShares, "price": priceNumber });
    }


    const buyHandler = async (event) => {
        event.preventDefault();
        setIsBuying(true);
        const symbol = props.symbol;
        const response = await buy(token, symbol, input.shares, props.price);
        if (response.data.status === "OK") {
            navigate("/portfolio");
        } else {
            setIsBuying(false);
            setError(response.data.error);
        }
    }

    const sellHandler = async (event) => {
        event.preventDefault();
        setIsSelling(true);
        const symbol = props.symbol;
        const response = await sell(token, symbol, input.shares, props.price);
        if (response.data.status === "OK") {
            navigate("/portfolio");
        } else {
            setIsSelling(false);
            setError(response.data.error);
        }
    }

    const watchlistHandler = async (event) => {
        event.preventDefault();
        const symbol = props.symbol;
        let response;

        if (event.target.checked) {
            response = await addWatch(token, symbol);
        } else {
            response = await removeWatch(token, symbol);
        }

        if (response.data.status === "OK") {
            updateIsWatched();
        } else {
            setError(response.data.error);
        }


    }

    const isAddedToWatchlist = async () => {
        const symbol = props.symbol;
        const response = await getWatch(token);
        if (response.data.status === "OK") {
            const watchedSymbols = response.data.data;
            for (let watchedSymbol of watchedSymbols) {
                if (watchedSymbol.symbol === symbol) {
                    return true;
                }
            }
            return false;
        } else {
            setError(response.data.error);
            return false;
        }
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setError(null);
    };

    return (
        <div className="center exo-font" style={{
            width: "300px",
            marginTop: "20px"
        }}>
            <form className="transactionForm">
                <div className="input-field">
                    <TextField
                        type="number"
                        name="shares"
                        label="Shares"
                        variant="outlined"
                        onChange={handleInputChange}
                        value={input.shares}
                        fullWidth
                    />
                </div>
                <div className="input-field">
                    <TextField
                        type="number"
                        name="price"
                        label="Price"
                        variant="outlined"
                        onChange={handleInputChange}
                        value={input.price}
                        fullWidth
                    />
                </div>
                <Snackbar
                    open={error !== null}
                    autoHideDuration={6000}
                    onClose={handleClose}
                    anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
                >
                    <Alert onClose={handleClose} severity="error" sx={{ width: '100%', fontSize: "1.2rem" }}>
                        {error}
                    </Alert>
                </Snackbar>
                <div style={{
                    position: "relative",
                    right: "200px"
                }}>
                    <FormGroup>
                        <FormControlLabel control={
                            <Switch
                                checked={isWatched}
                                onChange={watchlistHandler}
                                inputProps={{ 'aria-label': 'controlled' }}
                            />} label="Watch"
                            labelPlacement="start"
                        />
                    </FormGroup>
                </div>
                <div>
                    <LoadingButton
                        onClick={buyHandler}
                        loading={isBuying}
                        variant="contained"
                        size="large"
                        sx={{
                            position: "relative",
                            top: "30px",
                            right: "50px",
                            width: "100px",
                        }}
                    >
                        Buy
                    </LoadingButton>

                    <LoadingButton
                        loading={isSelling}
                        variant="contained"
                        onClick={sellHandler}
                        size="large"
                        sx={{
                            position: "relative",
                            top: "30px",
                            left: "50px",
                            width: "100px"
                        }}
                    >
                        Sell
                    </LoadingButton>
                </div>
            </form>
        </div>
    )
}

export default TransactionForm