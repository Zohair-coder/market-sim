import axios from 'axios';
import React, { useState } from 'react'
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Paper from '@mui/material/Paper';
import Navbar from "./Navbar"
import { useNavigate, Link } from 'react-router-dom';
import { loginUser } from '../api/auth';
import "./Register.css"

function Register() {

    const [input, setInput] = useState({
        "username": "",
        "password": "",
        "confirmPassword": ""
    });
    const [errors, setErrors] = useState([]);

    let navigate = useNavigate();

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setInput({ ...input, [name]: value });
    }

    const handleRegister = async (event) => {
        event.preventDefault();
        if (validateRegistration(input)) {
            await registerUser(input)
                .then(async function () {
                    await loginUser(input.username, input.password)
                        .then(function (response) {
                            if (response.data.status === "OK") {
                                const token = response.data.token;
                                localStorage.setItem("token", token);
                            }
                        })
                        .catch(function (err) {
                            console.log(err.response);
                        });
                    navigate("/portfolio");
                })
                .catch(err => {
                    setErrors(err.response.data);
                })
        }
    }

    const validateRegistration = (userData) => {
        const username = userData.username;
        const password = userData.password;
        const confirmPassword = userData.confirmPassword;
        let errors = [];

        errors = errors.concat(validateUsername(username));
        errors = errors.concat(validatePassword(password, confirmPassword));

        setErrors(errors);
        return errors.length === 0;
    }

    const validatePassword = (password, confirmPassword) => {
        let errors = [];

        if (password !== confirmPassword) {
            errors.push("Passwords do not match!");
        }

        if (password === undefined) {
            errors.push("Password is required");
        }

        if (password.length < 8) {
            errors.push("Password must be at least 8 characters long!");
        }

        return errors;
    }

    const validateUsername = (username) => {
        let errors = [];

        if (username === undefined) {
            errors.push("Username is required!");
        }

        if (username.length < 3) {
            errors.push("Username must be at least 3 characters!");
        }

        if (username.length > 20) {
            errors.push("Username must be less than 20 characters!");
        }

        if (username.includes(" ")) {
            errors.push("Username cannot contain spaces");
        }

        return errors;
    }

    const registerUser = (user) => {
        return axios.post("/api/auth/register", {
            username: user.username,
            password: user.password
        })
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setErrors([]);
    };

    return (
        <>
            <Navbar />
            <div className="center exo-font" style={{
                width: "500px",
                marginTop: "100px"
            }
            }>
                <Paper elevation={1} sx={{
                    padding: "30px 0px",
                    marginTop: "30px"
                }}>
                    <h1>Register</h1>
                    <form className="register-form">
                        <div className="input-container">
                            <TextField
                                type="text"
                                name="username"
                                label="Username"
                                variant="outlined"
                                onChange={handleInputChange}
                                value={input.username}
                                fullWidth
                            />
                        </div>
                        <div className="input-container">
                            <TextField
                                type="password"
                                name="password"
                                label="Password"
                                variant="outlined"
                                onChange={handleInputChange}
                                value={input.password}
                                fullWidth
                            />
                        </div>
                        <div className="input-container">
                            <TextField
                                type="password"
                                name="confirmPassword"
                                label="Confirm Password"
                                variant="outlined"
                                onChange={handleInputChange}
                                value={input.confirmPassword}
                                fullWidth
                            />
                        </div>
                        {errors.map(error => {
                            return <Snackbar
                                key={errors.indexOf(error)}
                                open={errors.length !== 0}
                                autoHideDuration={6000}
                                onClose={handleClose}
                                anchorOrigin={{ vertical: "bottom", horizontal: "center" }}>
                                <Alert
                                    key={errors.indexOf(error)}
                                    onClose={handleClose}
                                    severity="error"
                                    sx={{
                                        width: '100%',
                                        fontSize: "1.2rem"
                                    }}>
                                    {error}
                                </Alert>
                            </Snackbar>
                        })}
                        <Button
                            variant="text"
                            sx={{
                                position: "relative",
                                right: "100px",
                                top: "30px",
                            }}>
                            <Link to="/login">
                                Login
                            </Link>
                        </Button>

                        <Button
                            variant="contained"
                            type="submit"
                            onClick={handleRegister}
                            sx={{
                                position: "relative",
                                top: "30px",
                                left: "100px",
                            }}
                        >
                            Register
                        </Button>

                    </form>
                </Paper>
            </div>
        </>
    )
}

export default Register