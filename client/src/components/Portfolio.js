import React, { useState, useEffect } from 'react'
import { useNavigate } from "react-router-dom";
import "./Portfolio.css"
import Navbar from "./Navbar"
import PortfolioTable from "./PortfolioTable"
import WatchTable from "./WatchListTable"
import TradeTable from "./TradeTable"
import { getUserPortfolio } from "../api/portfolio"
import { getSubscriptions, subscribe, resubscribe, unsubscribe, getMarketData } from "../api/stocks"
import { removeWatch } from "../api/watch"
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import { Link } from "react-router-dom"
import AddIcon from '@mui/icons-material/Add';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import CloseIcon from '@mui/icons-material/Close';
import config from '../api_config';

function Portfolio() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [balance, setBalance] = useState({status: "loading", balance: 0, ntstatus: "loading", networth: 0});
  const [stocksList, setStocksList] = useState([]);
  const [watchList, setWatchList] = useState([]);
  const [trades, setTrades] = useState({tradesList: [], showSymbol: ""});
  const [marketData, setMarketData] = useState({prev: {}, cur: {}});

  const token = localStorage.getItem("token");
  let navigate = useNavigate();

  useEffect(() => {
    const updateSubscriptionList = async (stocksList) => {
      let subscriptions = (await getSubscriptions(token)).data.data;
      for (let stock of stocksList) {
        let symbol = stock.symbol;
        if (!subscriptions.includes(symbol)) {
          await subscribe(token, symbol, config["subscription_timeout"]);
        } else {
          resubscribe(token, symbol, config["subscription_timeout"]);
        }
      }
    }

    getUserPortfolio(token)
      .then(async function (response) {
        setIsAuthenticated(true);
        console.log(response.data);
        setBalance({status: "OK", balance: response.data.balance, ntstatus: "loading", networth: 0});
        setStocksList(response.data.stocks);
        setWatchList(response.data.watch);
        setTrades({tradesList: response.data.trades, showSymbol: ""});
        await updateSubscriptionList(response.data.stocks.concat(response.data.watch));
      }).catch(function (error) {
        console.log(error);
        navigate("/login");
      });
  }, []);


  useEffect(() => {
    const marketDataFrequency = 1000;
    const intervalId = setInterval(() => {
      getMarketData(token)
        .then((response) => {
          setMarketData({prev: marketData.cur, cur: response.data.data});
        }).catch((error) => {
          console.log(error);
        });
    }, marketDataFrequency);
    return () => clearInterval(intervalId);
  }, [marketData]);

  useEffect(() => {
    const intervalId = setInterval(() => {
      // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set
      const subscribedSymbols = new Set();
      stocksList.forEach(stock => subscribedSymbols.add(stock.symbol));
      watchList.forEach(watch => subscribedSymbols.add(watch.symbol));

      for (let symbol of subscribedSymbols.keys()) {
        resubscribe(token, symbol, config["subscription_timeout"]);
      }
    }, config["resubscribe_interval"]);

    return () => clearInterval(intervalId);
  }, [stocksList,watchList]);
  // necessary to update interval function whenever watchList or stockList updates

  useEffect(() => {
    let stockTotalList = stocksList.map(stock => (marketData.cur[stock.symbol] * stock.amount));
    let netWorthInStocks = stockTotalList.reduce((a,b) => a + b, 0);
    if (netWorthInStocks) { 
      setBalance({status: "OK", balance: balance.balance, ntstatus: "OK", networth: netWorthInStocks + balance.balance});
    } else {
      setBalance({status: "OK", balance: balance.balance, ntstatus: "OK", networth: balance.balance});
    }
  },[stocksList,marketData]);

  const removeFromWatchListOnClick = (e) => {
    let stockSymbol = e.target.getAttribute('stocksymbol')
    if (stockSymbol !== undefined) {
      removeWatch(token,stockSymbol);
      setWatchList(watchList.filter(watchList => watchList.symbol !== stockSymbol));
      console.log("Removed: " + stockSymbol + " from watchList");
      if (!stocksList.map(stock => stock.symbol).includes(stockSymbol)) {
        unsubscribe(token,stockSymbol);
        console.log("Unsubscribed: " + stockSymbol + " from server");
      }
    }
  }

  const showTradeTableOnClick = (e) => {
    let stockSymbol = e.target.getAttribute('stocksymbol');
    if ((stockSymbol !== undefined)) {
      // console.log(stockSymbol);
      setTrades({tradesList: trades.tradesList, showSymbol: stockSymbol});
    }
    // console.log(trades);
  }

  const hideTradeTableOnClick = (e) => {
    setTrades({tradesList: trades.tradesList, showSymbol: ""});
  }

  return (
    <>
      <Navbar />
      <div className='center exo-font'>
        <h3>Balance: {(balance.status === "loading") ? "loading..." : `$${balance.balance.toFixed(2)}`}</h3>
        <h3>Net Worth: {(balance.ntstatus === "loading") ? "loading..." : `$${balance.networth.toFixed(2)}`}</h3>
        <div className='portfolioContainer'>
          <h1 style={{alignSelf: 'center'}}>Portfolio</h1>
          <div style={{padding: '5px', alignSelf: 'right', position: 'absolute', top: '-1px', right:'2px'}}>
            <Tooltip disableFocusListener disableTouchListener title="To view trade history, hover over a symbol in your portfolio table and click.">
              <HelpOutlineIcon style={{color: 'lightgrey'}}/>
            </Tooltip>
          </div>
          {(stocksList === []) ? null : <PortfolioTable
            isAuthenticated={isAuthenticated}
            stocksList={stocksList}
            marketData={marketData}
            showTradeTableOnClick={showTradeTableOnClick}
          />}

          {(trades.showSymbol === "") ? null : 
          <div className='tradeContainer'>
            <h2>{trades.showSymbol} Trade History</h2>
            <CloseIcon onClick={hideTradeTableOnClick} className='tradeExit'></CloseIcon>
            <TradeTable
              isAuthenticated={isAuthenticated}
              tradeList={trades.tradesList.filter(trade => trade.symbol === trades.showSymbol)}
            />
          </div>}
        </div>

        <div className='watchListContainer'>
          <h1 style={{alignSelf: 'center'}}>Watch List</h1>
          <div style={{padding: '5px', alignSelf: 'right', position: 'absolute', top: '-1px', right:'2px'}}>
            <Tooltip disableFocusListener disableTouchListener title="To remove from watch list, hover over a symbol in your watch list and click.">
              <HelpOutlineIcon style={{color: 'lightgrey'}}/>
            </Tooltip>
          </div>
          {(watchList === []) ? null : <WatchTable
            isAuthenticated={isAuthenticated}
            watchList={watchList}
            marketData={marketData}
            removeFromWatchListOnClick={removeFromWatchListOnClick}
          />}
          <div style={{padding: '5px'}}>
          <Tooltip disableFocusListener disableTouchListener title="Add to watch list">
            <Link to="/trade" >
              <AddIcon style={{color: 'grey'}}/>
            </Link>
          </Tooltip>
          </div>
        </div>
      </div>
    </>
  )
}

export default Portfolio