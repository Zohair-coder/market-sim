import React, { useState, useEffect } from 'react'
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Paper from '@mui/material/Paper';
import "./Login.css"
import Navbar from './Navbar';
import { Link, Navigate } from "react-router-dom";
import { loginUser } from "../api/auth"

function Login() {

    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const [error, setError] = useState(null);
    const [input, setInput] = useState({
        "username": "",
        "password": "",
    });

    useEffect(() => {
        if (localStorage.getItem("token")) {
            setIsAuthenticated(true);
        }
    }, []);

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setInput({ ...input, [name]: value });
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        const username = input.username;
        const password = input.password;
        loginUser(username, password)
            .then(function (response) {
                if (response.data.status === "OK") {
                    const token = response.data.token;
                    localStorage.setItem("token", token);
                    setIsAuthenticated(true);
                } else {
                    setError(response.data.error);
                }
            })
            .catch(function (err) {
                console.log(err.response.data.error);
                if (err.response.data.error.includes("ECONNREFUSED")) {
                    setError("Server is not running!");
                } else {
                    setError(err.response.data.error);
                }
            });
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setError(null);
    };


    return (
        <>
            {isAuthenticated ? <Navigate to="/portfolio" /> : null}
            <Navbar />
            <div className="center exo-font" style={{
                width: "500px",
                marginTop: "100px"
            }}>
                <Paper elevation={1} sx={{
                    padding: "30px 0px",
                    marginTop: "30px"
                }}>
                    <h1>Login</h1>
                    <form className="login-form">
                        <div className="input-container">
                            <TextField
                                type="text"
                                name="username"
                                label="Username"
                                variant="outlined"
                                onChange={handleInputChange}
                                value={input.username}
                                fullWidth
                            />
                        </div>
                        <div className="input-container">
                            <TextField
                                type="password"
                                name="password"
                                label="Password"
                                variant="outlined"
                                onChange={handleInputChange}
                                value={input.password}
                                fullWidth
                            />

                        </div>
                        <Snackbar
                            open={error !== null}
                            autoHideDuration={6000}
                            onClose={handleClose}
                            anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
                        >
                            <Alert onClose={handleClose} severity="error" sx={{ width: '100%', fontSize: "1.2rem" }}>
                                {error}
                            </Alert>
                        </Snackbar>
                        <Button sx={{
                            position: "relative",
                            right: "100px",
                            top: "30px",
                        }}>
                            <Link to="/register">
                                Register
                            </Link>
                        </Button>

                        <Button
                            variant="contained"
                            type="submit"
                            onClick={handleSubmit}
                            sx={{
                                position: "relative",
                                top: "30px",
                                left: "100px",
                            }}
                        >
                            Login
                        </Button>
                    </form>
                </Paper>
            </div>
        </>
    )
}

export default Login;