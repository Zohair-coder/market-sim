import React, { useEffect, useState, useRef } from 'react'
import { Link } from "react-router-dom"
import { getUsername } from "../api/auth"
import PersonIcon from '@mui/icons-material/Person';
import "./Navbar.css"

function Navbar() {

    const [isAuthenticated, setIsAuthenticated] = useState(false);
    let username = useRef(null);

    useEffect(() => {
        const token = localStorage.getItem("token");
        if (token !== null) {
            getUsername(token)
                .then(function (response) {
                    if (response.data.status === "OK") {
                        username.current = response.data.username;
                        setIsAuthenticated(true);
                    } else {
                        setIsAuthenticated(false);
                    }
                })
        }

    }, []);

    return (
        <div id="navbar">
            <ul className="navbar">
                <li className="navbar-item"><Link to="/portfolio">Home</Link></li>
                <li className="navbar-item"><Link to="/about">About</Link></li>
                {isAuthenticated ? <li className="navbar-item"><Link to="/portfolio">Portfolio</Link></li> : null}
                {isAuthenticated ? <li className="navbar-item"><Link to="/trade">Trade</Link></li> : null}


                <div className="nav-center">
                    <li className="navbar-item"><Link to="/about">Market Sim<img className="logo"></img></Link></li>
                </div>

                <div className="nav-right">
                    {isAuthenticated ? <li className="navbar-item"><Link to="/logout">Logout</Link></li> : null}
                    {isAuthenticated ? <li className="navbar-item">
                        <Link to="/profile" className="active profile-link">
                            <PersonIcon />
                            <div>{username.current}</div>
                        </Link>
                    </li> : null}
                    {!isAuthenticated ? <li className="navbar-item"><Link to="/login" className="active">Login</Link></li> : null}
                    {!isAuthenticated ? <li className="navbar-item"><Link to="/register" className="active">Register</Link></li> : null}
                </div>
            </ul >
        </div >
    )
}

export default Navbar
