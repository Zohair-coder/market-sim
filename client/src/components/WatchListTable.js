import React, { useState, useEffect } from 'react'
import "./Portfolio.css"

function StockRow(stockProp) {
    let current_price;

    if (stockProp['current_price'] === undefined) {
        current_price = "-";
    } else {
        current_price = stockProp['current_price'].toFixed(2);
    }

    function changeColor(e) {
        e.target.className = 'watchBtnDel';
    }

    function revertColor(e) {
        e.target.className = 'watchBtn';
    }

    return (
        <tr>
            <td 
                stocksymbol={stockProp.symbol}
                onMouseOver={changeColor}
                onMouseLeave={revertColor}
                onClick={stockProp.removeFromWatchListOnClick}>
                {stockProp.symbol}
            </td>
            <td>{stockProp.name}</td>
            <td style={((stockProp.prev_price === stockProp.current_price) || (stockProp.prev_price === undefined)) ? 
            {color: 'black'} : (stockProp.prev_price < stockProp.current_price) ? 
            {color: "green"} : {color: "red"}}>${current_price}</td>
        </tr>
    );
}

function WatchTable(prop) {
    let tablerows = null;

    if (prop.isAuthenticated && prop.watchList && (Object.keys(prop.marketData).length > 0)) {
        tablerows = prop.watchList.map(stock => 
            <StockRow 
                key={stock.wid}
                symbol={stock.symbol}
                name={stock.symbol}
                current_price={prop.marketData.cur[stock.symbol]}
                prev_price={prop.marketData.prev[stock.symbol]}
                removeFromWatchListOnClick={prop.removeFromWatchListOnClick}
            />);
    }

    return (
        <table>
            <thead>
                <tr>
                    <th>Symbol</th>
                    <th>Name</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody id="watch-table-body">
                {tablerows}
            </tbody>
        </table>
    );
}

export default WatchTable