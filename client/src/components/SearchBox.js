import React, { useState, useEffect } from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import { getSymbols } from '../api/stocks';

export default function ComboBox(props) {

    const [allSymbols, setAllSymbols] = useState([]);
    const [displayedSymbols, setDisplayedSymbols] = useState([]);

    useEffect(() => {
        const token = localStorage.getItem('token');
        getSymbols(token).then(res => {
            if (res.data.status === "OK") {
                setAllSymbols(res.data.data);
            } else {
                console.log("Error: " + res.data.message);
            }
        });
    }, []);

    const updateSymbols = (event, value) => {
        value = value.toUpperCase();
        let newSymbols = [];
        const maxSymbols = 5;
        let count = 0;
        for (let symbol of allSymbols) {
            if (symbol.startsWith(value)) {
                count++;
                newSymbols.push(symbol);
            }
            if (count >= maxSymbols - 1) {
                break;
            }
        }
        setDisplayedSymbols(newSymbols);

    };

    return (
        <Autocomplete
            disablePortal
            autoHighlight
            id="combo-box-demo"
            options={displayedSymbols.length === 0 ? ["Search for symbols"] : displayedSymbols}
            sx={{ width: 300, marginTop: "30px;" }}
            renderInput={(params) => <TextField {...params} label="Symbol" />}
            onInputChange={updateSymbols}
            onChange={props.searchFunc}
            className="center"
        />
    );
}
