const config = {
    "subscription_timeout": 30000,
    "resubscribe_interval": 20000,
    "market_data_frequency": 1000
}

export default config;