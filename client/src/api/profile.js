import axios from "axios"

function getUserBalance(token) {
    const config = {
        method: 'get',
        url: '/api/profile',
        headers: {
            'x-auth': token
        }
    };

    return axios(config);
}

export { getUserBalance };