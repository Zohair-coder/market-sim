import axios from "axios"


function buy(token, stock, amount, price) {
    const config = {
        method: 'post',
        url: '/api/trade/buy',
        headers: {
            'x-auth': token,
            "stock": stock,
            "amount": amount,
            "price": price
        }
    };

    return axios(config);
}

function sell(token, stock, amount, price) {
    const config = {
        method: 'post',
        url: '/api/trade/sell',
        headers: {
            'x-auth': token,
            "stock": stock,
            "amount": amount,
            "price": price
        }
    };

    return axios(config);
}

export { buy, sell };