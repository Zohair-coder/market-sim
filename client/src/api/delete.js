import axios from "axios"

function deleteAccount(token) {
    const config = {
        method: 'post',
        url: '/api/delete',
        headers: {
            'x-auth': token
        }
    };
    return axios(config);
}

export { deleteAccount };