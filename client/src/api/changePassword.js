import axios from "axios"

function checkPassword(token, password) {

    const config = {
        method: 'get',
        url: '/api/update',
        headers: {
            'x-auth': token,
            'password': password
        }
    };

    return axios(config);
}

export { checkPassword };