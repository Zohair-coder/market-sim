import axios from "axios"


function addWatch(token, stock) {
    const config = {
        method: 'post',
        url: '/api/watch/add',
        headers: {
            'x-auth': token,
            "stock": stock
        }
    };

    return axios(config);
}

function removeWatch(token, stock) {
    const config = {
        method: 'post',
        url: '/api/watch/remove',
        headers: {
            'x-auth': token,
            "stock": stock
        }
    };

    return axios(config);
}

function getWatch(token) {
    const config = {
        method: 'get',
        url: '/api/watch/get',
        headers: {
            'x-auth': token,
        }
    };

    return axios(config);
}

export { addWatch, removeWatch, getWatch };