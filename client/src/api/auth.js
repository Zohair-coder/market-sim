import axios from "axios"

function loginUser(username, password) {
    const data = JSON.stringify({
        "username": username,
        "password": password
    });

    const config = {
        method: 'post',
        url: '/api/auth/login',
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
    };

    return axios(config);
}

function getUsername(token) {
    const config = {
        method: 'get',
        url: '/api/auth/verify',
        headers: {
            'x-auth': token
        }
    };

    return axios(config);
}

export { loginUser, getUsername };