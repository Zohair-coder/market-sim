import axios from "axios"

function getUserPortfolio(token) {
    const config = {
        method: 'get',
        url: `/api/portfolio`,
        headers: {
            'x-auth': token
        }
    };

    return axios(config);
}

export { getUserPortfolio };