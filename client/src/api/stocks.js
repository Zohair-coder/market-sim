import axios from "axios"


function getSymbols(token) {
    const config = {
        method: 'get',
        url: '/api/stocks/symbols',
        headers: {
            'x-auth': token
        }
    };

    return axios(config);
}

function getStocks(token) {
    const config = {
        method: 'get',
        url: '/api/stocks/stocks',
        headers: {
            'x-auth': token
        }
    };

    return axios(config);
}

function getMarketData(token) {
    const config = {
        method: 'get',
        url: '/api/stocks/marketdata',
        headers: {
            'x-auth': token
        }
    };

    return axios(config);
}

function getSubscriptions(token) {
    const config = {
        method: 'get',
        url: '/api/stocks/subscribed',
        headers: {
            'x-auth': token
        }
    };

    return axios(config);
}

function getQuote(token, symbol) {
    const config = {
        method: 'get',
        url: '/api/stocks/quote',
        headers: {
            'x-auth': token,
            'symbol': symbol
        }
    };

    return axios(config);
}

function resubscribe(token, symbol, timeout = 300000) {
    const config = {
        method: 'post',
        url: '/api/stocks/resubscribe',
        headers: {
            'x-auth': token,
            'symbol': symbol,
            'timeout': timeout

        }
    };

    return axios(config);
}

function subscribe(token, symbol, timeout = 300000) {
    const config = {
        method: 'post',
        url: '/api/stocks/subscribe',
        headers: {
            'x-auth': token,
            'symbol': symbol,
            'timeout': timeout

        }
    };

    return axios(config);
}

function unsubscribe(token, symbol) {
    const config = {
        method: 'post',
        url: '/api/stocks/unsubscribe',
        headers: {
            'x-auth': token,
            'symbol': symbol

        }
    };

    return axios(config);
}

export { getSymbols, getStocks, getSubscriptions, getQuote, subscribe, unsubscribe, resubscribe, getMarketData };